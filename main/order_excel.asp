<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<meta charset="utf-8"/>
<%
Dim sql, rs, q, count_member, yearmonth

dim year1 	        : year1 	= RequestS("year1")
dim month1 	        : month1 	= RequestS("month1")

yearmonth = year1 & month1


Dim excel_filename
excel_filename = "주문이벤트_" & year1 & "_" & month1

Response.Buffer=true
Response.ContentType = "application/vnd.ms-excel;charset=utf-8"
Response.CacheControl = "public"
Response.AddHeader "Content-Disposition", "attachment;filename="&Server.URLPathEncode(excel_filename)&".xls"
%>
<table border=1>
	<tr>
		<th height=30>번호</th>
		<th>안과코드</th>
		<th>안과명</th>
		<th>담당자명</th>
		<th>담당자 휴대폰번호</th>
		<th>전화번호</th>
		<th>팩스</th>
		<th>주소</th>
		<th>목표주문건수</th>
		<th>실제주문건수</th>
	  </tr>
<%
Call DbOpen()

sql = "SELECT count(at.intSeq) FROM orderevent as at inner join denlist as bt on at.den_code=bt.dencode where at.counting >= bt.order_num and at.yearmonth = '"& yearmonth &"'"
Set rs = dbconn.execute(sql)

  count_member = rs(0)

rs.close
Set rs= Nothing

sql = "SELECT at.den_code, at.yearmonth, at.counting, bt.denname, bt.denaddress, bt.denaddress2, bt.denmanager, bt.tel1, bt.tel2, bt.tel3, bt.hp1, bt.hp2, bt.hp3, bt.fax1, bt.fax2, bt.fax3, bt.order_num FROM orderevent as at inner join denlist as bt on at.den_code=bt.dencode where at.counting >= bt.order_num and at.yearmonth = '"& yearmonth &"' order by at.intseq desc"
Set rs = dbconn.execute(sql)

If rs.eof Then
%>
    <tr>
	    <td colspan="10"></td>
	</tr>
<%
Else
  q=1
  Do while Not rs.eof
%>
	<tr>
		<td height=30><%=count_member-q+1%></td>
		<td><%=rs("den_code")%></td>
		<td><%=rs("denname")%></td>
		<td><%=rs("denmanager")%></td>
		<td><%=rs("hp1")%>-<%=rs("hp2")%>-<%=rs("hp3")%></td>
		<td><%=rs("tel1")%>-<%=rs("tel2")%>-<%=rs("tel3")%></td>
		<td><%=rs("fax1")%>-<%=rs("fax2")%>-<%=rs("fax3")%></td>
		<td><%=rs("denaddress")%>&nbsp;<%=rs("denaddress2")%></td>
		<td><%=rs("order_num")%>건</td>
		<td><%=rs("counting")%>건</td>
    </tr>
<%
  q=q+1
  rs.movenext
  Loop
  rs.close()
  Set rs = Nothing
End If
Call DbClose()
%>
</table>