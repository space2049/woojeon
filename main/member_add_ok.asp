<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Dim tablename
tablename = "mTb_Member2"
Const UploadFolder = "/upload/member"

'+++++++[0] 입력은 insert 수정은 update 삭제는 delete 로 변경한다.
Dim insert

set insert =  new DextQueryClass

Call insert.setUploadFolder(UploadFolder)

'+++++++[1] 넘어오는 값들을 확인해 본다.넘어오는 값확인후 반드시 주석처리로 바꾼다
'삭제시에는 아래 3값이 활성화 되어있으면 에러남
'=====================================================================
'Call insert.ViewFormName()  	'[주석]폼네임명을 볼수있는 함수
'Call insert.ViewTbaleGride()	'[주석]테이블로 보기
'Call insert.ViewCreateTable()	'[주석]테이블쿼리
'Call insert.ViewFiledInsert
'Call insert.ViewFiledupdate
'=====================================================================
'+++++++[0]_End

'+++++++[2] 수정과 삭제의 경우만 활설화 된다. 입력(insert)때는 비활성화
'Call insert.setwhere(" intSeq = '"& request.QueryString("intSeq") &"' ")	'//수정과 삭제에서만 사용
'+++++++[2]_End

'// 기본으로 넘어오는 값 세팅
'Call insert.setDefaultData("")

'+++++++[3] 입력페이지 이외에는 [주석]처리한다.
'Call insert.setdataAdd("strCategory", request.QueryString(ProgramFolderName))
'+++++++[3]_End

Call insert.settablename(tablename)

Call DbOpen()

Dim strId, strId1

If Mid(primaryval,5,2) = "01" Then
  strId1 = "A"
elseIf Mid(primaryval,5,2) = "02" Then
  strId1 = "B"
elseIf Mid(primaryval,5,2) = "03" Then
  strId1 = "C"
elseIf Mid(primaryval,5,2) = "04" Then
  strId1 = "D"
elseIf Mid(primaryval,5,2) = "05" Then
  strId1 = "E"
elseIf Mid(primaryval,5,2) = "06" Then
  strId1 = "F"
elseIf Mid(primaryval,5,2) = "07" Then
  strId1 = "G"
elseIf Mid(primaryval,5,2) = "08" Then
  strId1 = "H"
elseIf Mid(primaryval,5,2) = "09" Then
  strId1 = "I"
elseIf Mid(primaryval,5,2) = "10" Then
  strId1 = "J"
elseIf Mid(primaryval,5,2) = "11" Then
  strId1 = "K"
elseIf Mid(primaryval,5,2) = "12" Then
  strId1 = "L"
End If

Dim num1, i
For i = 1 to 3
  Randomize                    '//랜덤을 초기화 한다.
  num1 = num1 & CInt(Rnd*9)    '//랜덤 숫자를 만든다. 
Next

strId = strId1 & Mid(primaryval,3,2) & Mid(primaryval,7,2) & num1 & Mid(primaryval,11,4)

Call insert.setdataAdd("strHp", insert.getFormValue("istrHpt"))
Call insert.setdataAdd("strPwd", Encrypt_Sha(insert.getFormValue("istrPwdt")))
Call insert.setdataAdd("strName", insert.getFormValue("istrNamet"))
Call insert.setdataAdd("strId", strId)
Call insert.setdataAdd("strbirthday1", insert.getFormValue("istrbirthdayt1"))
Call insert.setdataAdd("strbirthday2", insert.getFormValue("istrbirthdayt2"))

Call insert.setdataAdd("dencode", insert.getFormValue("dencode"))
Call insert.setdataAdd("denname", insert.getFormValue("denname"))

Call insert.setdataAdd("intPermit", insert.getFormValue("iintpermitt"))
Call insert.setdataAdd("strmemo", insert.getFormValue("istrmemot"))
Call insert.setdataAdd("dtmInsertDate", FormatDateTime(now(),2) & " " & FormatDateTime(now(),4))

'+++++++[5] form에서 넘어오는 업로드될 파일 값 세팅 [함수명 바뀜insert-update-delete]
	call insert.setFileData("file_1","insert")
	'Call insert.setinsertNumFileDataAdd("strImage") '//구번전 파일 업로드
'+++++++[5]_End

  dim strSql
  strSQL = insert.getqueryinsert

 'Response.write strSQL
 ' Response.end

  ' Response.end

  Call AdoConnExecute(strSQL)

Call DbClose()

set insert = nothing

'+++++++[6] 메세지를 바꿔주고 처리후 이동할 페이지를 정해준다.
Call jsAlertMsgUrl("등록되었습니다", "member_list.asp")
'Call jsAlertMsgUrl("등록되었습니다", "member_add.asp?"&GetString(""))
'+++++++[6]_End
%>