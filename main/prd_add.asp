<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<meta charset="utf-8"/>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<link rel="stylesheet" href="_css/sub.css" />
<script>
$(document).ready(function() {
	// 첨부파일
	var fileTarget = $('.file_box .upload_hidden');
	fileTarget.on('change', function(){ 
		if(window.FileReader){
			var filename = $(this)[0].files[0].name;
		}
		else {
			var filename = $(this).val().split('/').pop().split('\\').pop(); 
		}
		$(this).siblings('.upload_name').val(filename);
	});
});
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">제품등록</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">제품관리</a></li>
		            <li><a href="#">제품등록</a></li>
				</ul>
			</div>
			<form class="form_m_add" name="frmRequestForm" method="post" onSubmit="return frmRequestForm_Submit(this);" action="prd_add_ok.asp?<%=getstring("")%>" enctype="multipart/form-data">
				<div class="section">
					<div class="table_tit">
						<h3>기본정보</h3>
					</div>
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: 488px;">
							<col style="width: 280px;">
							<col style="width: 488px;">
						</colgroup>
						<tbody>
							<tr>
								<th><em>*</em> 제품카테고리</th>
								<td colspan="3">
								    <%
									call dbopen
									Dim sql, rs
									sql = "SELECT * FROM goodscategory order by intSeq asc"
									set rs = dbconn.execute(sql)
									%>
									<select name="category_num">
									    <option value="">제품카테고리선택</option>
									<%
									if not rs.eof Then

									  Do while not rs.eof
									%>
										<option value="<%=rs("intSeq")%>"><%=rs("cate_name")%></option>
									<%
									  rs.movenext
									  Loop

									End if
									%>
									</select>
									<%
									rs.close
									Set rs=Nothing
									Call DbClose()
									%>
								</td>
							</tr>
							<tr>
								<th><em>*</em> 제품명</th>
								<td><input type="text" name="goods_name" id="goods_name"></td>
								<th>제품코드</th>
								<td><input type="text" name="goods_code" id="goods_code"></td>
							</tr>
							<tr>
								<th><em>*</em> 제품이미지 <span>(996*300)</span></th>
								<td>
									<div class="file_box">
										<input class="upload_name" disabled="disabled">
										<input type="file" id="write_file" name="file_1" class="upload_hidden">
										<label for="write_file">파일첨부</label>
									</div>
								</td>
							</tr>
							<tr>
								<th><em>*</em> 진열상태</th>
								<td colspan="3" class="chk_wrap">
									<div class="chk_box">
										<input id="prd_status01" type="radio" name="state_yn" value="Y" />
										<label for="prd_status01"><span></span><p>진열함</p></label>
									</div>
									<div class="chk_box">
										<input id="prd_status02" type="radio" name="state_yn" value="N" />
										<label for="prd_status02"><span></span><p>진열안함</p></label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="section">
					<div class="table_tit">
						<h3>옵션설정</h3>
					</div>
					<table class="table_write">
						<colgroup>
							<col style="width: 272px;">
							<col style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th class="vt_mid">옵션입력</th>
								<td>
									<table class="table_write table_prd">
										<colgroup>
											<col style="width: 225px;">
											<col style="width: 225px;">
											<col style="width: *;">
											<col style="width: 225px;">
										</colgroup>
										<thead>
											<tr>
												<th>옵션타입</th>
												<th>옵션타입</th>
												<th>옵션항목(입력형, 날짜형은 옵션항목을 입력하지 않습니다)</th>
												<th>Table column</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<select name="option_type1">
														<option value="">옵션타입</option>
														<option value="a">범주형</option>
														<option value="b">기본형</option>
														<option value="c">날짜형</option>
														<option value="d">입력형</option>
													</select>
												</td>
												<td><input type="text" name="option_name1" id="option_name1" style="width:250px;" placeholder="옵션명입력"></td>
												<td><input type="text" name="option_contents1" id="option_contents1" placeholder="예시 : 1,2,3… (,)콤마로 구분"></td>
												<td><input type="text" name="" style="width:220px;" value="option_contents1"></td>
											</tr>
											<tr>
												<td>
													<select name="option_type2">
														<option value="">옵션타입</option>
														<option value="a">범주형</option>
														<option value="b">기본형</option>
														<option value="c">날짜형</option>
														<option value="d">입력형</option>
													</select>
												</td>
												<td><input type="text" name="option_name2" id="option_name2" style="width:250px;" placeholder="옵션명입력"></td>
												<td><input type="text" name="option_contents2" id="option_contents2" placeholder="예시 : 1,2,3… (,)콤마로 구분"></td>
												<td><input type="text" name="" style="width:220px;" value="option_contents2"></td>
											</tr>
											<tr>
												<td>
													<select name="option_type3">
														<option value="">옵션타입</option>
														<option value="a">범주형</option>
														<option value="b">기본형</option>
														<option value="c">날짜형</option>
														<option value="d">입력형</option>
													</select>
												</td>
												<td><input type="text" name="option_name3" id="option_name3" style="width:250px;" placeholder="옵션명입력"></td>
												<td><input type="text" name="option_contents3" id="option_contents3" placeholder="예시 : 1,2,3… (,)콤마로 구분"></td>
												<td><input type="text" name="" style="width:220px;" value="option_contents3"></td>
											</tr>
											<tr>
												<td>
													<select name="option_type4">
														<option value="">옵션타입</option>
														<option value="a">범주형</option>
														<option value="b">기본형</option>
														<option value="c">날짜형</option>
														<option value="d">입력형</option>
													</select>
												</td>
												<td><input type="text" name="option_name4" id="option_name4" style="width:250px;" placeholder="옵션명입력"></td>
												<td><input type="text" name="option_contents4" id="option_contents4" placeholder="예시 : 1,2,3… (,)콤마로 구분"></td>
												<td><input type="text" name="" style="width:220px;" value="option_contents4"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="btns">
					<button type="button" class="btn_gray" onclick="location.href='prd_list.asp';">목록</button>
					<button type="button" class="btn_blue" onClick="this.form.onsubmit();">저장</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<script language="javascript">
<!--
function frmRequestForm_Submit(frm){

  if (frm.category_num.value == "" ) {
    alert("제품카테고리를 선택하세요.");
	return;
  }

  if ( frm.goods_name.value == "" ) {
    alert("제품명을 입력하세요.");
	$("#goods_name").focus();
	return;
  }

  if ( frm.file_1.value == "" ) {
    alert("제품이미지를 입력하세요.");
	return;
  }

  if ( frm.state_yn[0].checked == false && frm.state_yn[1].checked == false ) {
    alert("진열상태를 체크하세요.");
	return;
  }

  frm.submit();
}

function f_is_alpha( it ) {
  var alpha ='-';
  var numeric = '1234567890';
  var blank = ' ';
  var nonkorean = numeric;
  var i ;
  var t = it.value ;
  for ( i=0; i<t.length; i++ )
    if( nonkorean.indexOf(t.substring(i,i+1)) < 0) {
      break ;
    }
    if ( i != t.length ) {
      return true ;
    }
  return ;
}
//-->
</script>