<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Const UploadFolder = "/upload/goods"
Const tablename = "goodslist"

Dim category_num    : category_num    = RequestS("category_num")
dim gum1 	        : gum1 	          = RequestS("gum1")
dim gum2 	        : gum2 	          = RequestS("gum2")
dim state_yn 	    : state_yn 	      = RequestS("state_yn")

dim intTotalCount, intTotalPage

dim intNowPage			: intNowPage 		= Request.QueryString("page")
dim intPageSize			: intPageSize 		= 10
dim intBlockPage		: intBlockPage 		= 10

dim query_filde			: query_filde		= "*"
dim query_Tablename		: query_Tablename	= TableName
dim query_where			: query_where		= " intSeq > 0 "
dim query_orderby		: query_orderby		= "order by intseq desc"

if category_num <> "" then
	query_where = query_where &" and category_num = "& category_num &""
end If

if Len(gum2) > 0 then
	query_where = query_where &" and "& gum1 & " like '%" & gum2 & "%'"
end If

if state_yn <> "" then
	query_where = query_where &" and state_yn = '"& state_yn &"'"
end If

call intTotal

dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

dim sql, rs, rs3, count_, rs4, count2_
sql = getQuery
call dbopen
set rs = dbconn.execute(sql)

set rs3 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where intSeq > 0")
count_ = 0
if  rs3.eof then
Else
  count_ = rs3("c")
End If
rs3.close()
set rs3 = Nothing

If gum2 <> "" Or category_num <> "" Or state_yn <> "" then
  set rs4 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where "& query_where &"")
  count2_ = 0
  if  rs4.eof then
  Else
    count2_ = rs4("c")
  End If
  rs4.close()
  set rs4 = Nothing
End If
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="_css/sub.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
	$( "#period_start" ).datepicker({
		showOn: "button",
		buttonImage: "images/sub/ic_cal_g.svg",
		buttonImageOnly: true,
		buttonText: "날짜 선택",
		dateFormat: "yy.mm.dd"
	});
	$( "#period_end" ).datepicker({
		showOn: "button",
		buttonImage: "images/sub/ic_cal_g.svg",
		buttonImageOnly: true,
		buttonText: "날짜 선택",
		dateFormat: "yy.mm.dd"
	});
});

function frmRequestForm_Submit2(frm) {
  var frm = document.frm;
  if (frm.category_num.value == "" && frm.gum2.value == "" && frm.state_yn[1].checked == false && frm.state_yn[2].checked == false){ alert("검색어, 진열상태 중 하나 이상을 입력하거나 선택하세요."); frm.gum2.focus(); return;	}
  frm.submit();
}

function prd_delete(a) {

  sub = confirm("삭제하시겠습니까.")
  if (sub == false) {
    return;
  }

  document.fmList_b.intSeq.value = a;
  document.fmList_b.action = "prd_list_delete.asp";
  document.fmList_b.submit();

}
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

<form name="fmList_b" method="get">
<input type="hidden" name="intSeq" />
<input type="hidden" name="page" value="<%=Request.QueryString("page")%>" />
</form>

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">제품목록</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">제품관리</a></li>
		            <li><a href="#">제품목록</a></li>
				</ul>
			</div>
				<form class="member_form" name="frm" action="?<%=getstring("")%>" method="get" >
				<div class="search_area">
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th>검색어</th>
								<td class="s_word">
									<%
									Dim sql5, rs5
									sql5 = "SELECT * FROM goodscategory order by intSeq asc"
									set rs5 = dbconn.execute(sql5)
									%>
									<select name="category_num">
									    <option value="">제품카테고리선택</option>
									<%
									if not rs5.eof Then

									  Do while not rs5.eof
									%>
										<option value="<%=rs5("intSeq")%>" <%If category_num <> "" then%><%If CInt(category_num) = rs5("intSeq") then%>selected<%End if%><%End if%>><%=rs5("cate_name")%></option>
									<%
									  rs5.movenext
									  Loop

									End if
									%>
									</select>
									<%
									rs5.close
									Set rs5=Nothing
									%>
									<select name="gum1">
										<option value="goods_name" <%If gum1 = "goods_name" then%>selected<%End if%>>제품명</option>
										<option value="goods_code" <%If gum1 = "goods_code" then%>selected<%End if%>>제품코드</option>
									</select>
									<input type="text" name="gum2" id="gum2" value="<%=gum2%>" />
								</td>
							</tr>
							<tr>
								<th>진열상태</th>
								<td class="chk_wrap">
									<div class="chk_box">
										<input id="prd_status_all" type="radio" name="state_yn" value="" checked />
										<label for="prd_status_all"><span></span><p>전체</p></label>
									</div>
									<div class="chk_box">
										<input id="prd_status01" type="radio" name="state_yn" value="Y" <%If state_yn = "Y" then%>checked<%End if%> />
										<label for="prd_status01"><span></span><p>진열함</p></label>
									</div>
									<div class="chk_box">
										<input id="prd_status02" type="radio" name="state_yn" value="N" <%If state_yn = "N" then%>checked<%End if%> />
										<label for="prd_status02"><span></span><p>진열안함</p></label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="btns">
						<button type="button" class="btn_blue" onclick="frmRequestForm_Submit2();">검색</button>
					</div>
				</div>
				</form>
				<div class="board_num">
					<p><%If gum2 <> "" Or category_num <> "" Or state_yn <> "" then%>검색 <%=count2_%> / <%End if%>전체 <%=count_%></p>
					<div class="right_btns">
						<button type="button" class="btn_add" onclick="location.href='prd_add.asp'">등록</button>
					</div>
				</div>
				<form name="dFrm" method="post">
				<table class="table_list">
					<colgroup>
						<col style="width: 120px;">
						<col style="width: 120px;">
						<col style="width: 200px;">
						<col style="width: *;">
						<col style="width: 256px;">
						<col style="width: 200px;">
						<col style="width: 200px;">
						<col style="width: 200px;">
					</colgroup>
					<thead>
						<th>
						  <div class="chk_box">
						    <input type="checkbox" name="all_chk" id="all_chk" value=0 onclick="CheckAll();">
						    <label for="all_chk"><span></span></label>
						  </div>
					    </th>
						<th>번호</th>
						<th>제품코드</th>
						<th>제품명</th>
						<th>제품카테고리</th>
						<th>진열상태</th>
						<th>등록일</th>
						<th>관리</th>
					</thead>
					<tbody>
					    <%
						Dim sql6, rs6, category_name, state_name
						if not rs.eof Then

						  rs.move MoveCount
						  Do while not rs.eof

						    sql6 = "SELECT cate_name FROM goodscategory WHERE intSeq = '"&rs("category_num")&"'"
                            set rs6 = dbconn.execute(sql6)

							  category_name = rs6(0)

							rs6.close
							Set rs6=Nothing
							
							If rs("state_yn") = "Y" Then
							  state_name = "진열함"
							Else
							  state_name = "진열안함"
							End if
					    %>
						<tr>
							<td>
							  <div class="chk_box">
								<input type="checkbox" id="chk_<%=intNowNum%>" name="ChkCheck" value="<%=rs("intSeq")%>" />
								<label for="chk_<%=intNowNum%>"><span></span></label>
							  </div>
							</td>

							<td><%=intNowNum%></td>
							<td><%=rs("goods_code")%></td>
							<td><%=rs("goods_name")%></td>
							<td><%=category_name%></td>
							<td><%=state_name%></td>
							<td><%=Left(rs("insertdate"),10)%></td>
							<td>
								<button type="button" class="btn_s btn_del" onclick="javascript:prd_delete('<%=rs("intSeq")%>');">삭제</button>
								<button type="button" class="btn_s btn_modify" onclick="location.href='prd_edit.asp?intSeq=<%=rs("intSeq")%>&page=<%=Request.QueryString("page")%>';">수정</button>
							</td>
						</tr>
						<%
						  intNowNum = intNowNum - 1
						  rs.movenext
						  Loop

						Else
					    %>
					    <tr>
						  <td colspan="8">목록이 없습니다.</td>
					    </tr>
					    <%
						end if

						rs.close
						set rs = nothing
					    %>
					</tbody>
				</table>
				<div class="table_batch">
					<p>선택한 제품을</p>
					<select name="state_yn">
						<option value="Y">진열</option>
						<option value="N">진열안함</option>
					</select>
					<button type="button" onclick="del_page()">일괄처리</button>
				</div>
				<input type="hidden" name="ChkCheck" />
			    </form>
			    <%call Paging_list("")%>
		</div>
	</div>
</div>
</body>
</html>
<%
Call DbClose()
%>

<script>
function del_page(){

  var objForm = document.dFrm;
  var len = objForm.ChkCheck.length
  var poll = "0"
  var poll1 = ""
  var poll2 = ""
  var state_yn = objForm.state_yn.value;

  for(i = 0; i < len; i++) {

    if(objForm.ChkCheck[i].checked) {
      poll=objForm.ChkCheck[i].value;
	  poll2 = poll + ",";
	  poll1 = poll1 + poll2;
	}
  }

  if (poll=="0"){
    alert("일괄처리할 목록에 체크하세요.");
	return;
  }

  location.href="prd_list_change.asp?sms_no="+poll1+"&page=<%=Request.QueryString("page")%>&state_yn="+state_yn;
}

var flag = true;
function CheckAll(checkFlag){

  for(i = 0; i < document.dFrm.elements.length; ++i){

	if(document.dFrm.elements[i].name == 'ChkCheck'){
	  if (document.dFrm.elements[i].value == 'on' )
		document.dFrm.elements[i].checked = false ;
	  else
		document.dFrm.elements[i].checked = flag  ;
	}
  }
  if(flag == true){
	flag = false; // document.frmSearch.btnAll.value = "전체해제";
  }
  else{
	flag = true;  // document.frmSearch.btnAll.value = "전체선택";
  }
}

</script>