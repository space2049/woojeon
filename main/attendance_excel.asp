<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<meta charset="utf-8"/>
<%
Dim sql, rs, q, count_member, yearmonth

dim year1 	        : year1 	= RequestS("year1")
dim month1 	        : month1 	= RequestS("month1")

yearmonth = year1 & month1


Dim excel_filename
excel_filename = "출석체크이벤트_" & year1 & "_" & month1

Response.Buffer=true
Response.ContentType = "application/vnd.ms-excel;charset=utf-8"
Response.CacheControl = "public"
Response.AddHeader "Content-Disposition", "attachment;filename="&Server.URLPathEncode(excel_filename)&".xls"
%>
<table border=1>
	<tr>
	    <th height=30>번호</th>
		<th>안과코드</th>
		<th>안과명</th>
		<th>이름</th>
		<th>휴대폰아이디</th>
		<th>생일</th>
		<th>방문수</th>
	  </tr>
<%
Call DbOpen()

sql = "SELECT count(at.intSeq) FROM checkevent as at inner join mTb_Member2 as bt on at.member_code=bt.strId where at.counting >= 20 and at.yearmonth = '"& yearmonth &"'"
Set rs = dbconn.execute(sql)

  count_member = rs(0)

rs.close
Set rs= Nothing

sql = "SELECT at.member_code, at.yearmonth, at.counting, bt.strName, bt.strHp, bt.strbirthday1, bt.strbirthday2, bt.dencode, bt.denname FROM checkevent as at inner join mTb_Member2 as bt on at.member_code=bt.strId where at.counting >= 20 and at.yearmonth = '"& yearmonth &"' order by at.intseq desc"
Set rs = dbconn.execute(sql)

If rs.eof Then
%>
    <tr>
	    <td colspan="7"></td>
	</tr>
<%
Else
  q=1
  Do while Not rs.eof
%>
	<tr>
	    <td height=30><%=count_member-q+1%></td>
		<td><%=rs("dencode")%></td>
		<td><%=rs("denname")%></td>
		<td><%=rs("strName")%></td>
		<td><%=rs("strHp")%></td>
		<td><%=rs("strbirthday1")%>월 <%=rs("strbirthday2")%>일</td>
		<td><%=rs("counting")%></td>
    </tr>
<%
  q=q+1
  rs.movenext
  Loop
  rs.close()
  Set rs = Nothing
End If
Call DbClose()
%>
</table>