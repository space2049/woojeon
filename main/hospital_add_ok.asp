<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Dim tablename
tablename = "denlist"
Const UploadFolder = "/upload/member"

'+++++++[0] 입력은 insert 수정은 update 삭제는 delete 로 변경한다.
Dim insert

set insert =  new DextQueryClass

Call insert.setUploadFolder(UploadFolder)

'+++++++[1] 넘어오는 값들을 확인해 본다.넘어오는 값확인후 반드시 주석처리로 바꾼다
'삭제시에는 아래 3값이 활성화 되어있으면 에러남
'=====================================================================
'Call insert.ViewFormName()  	'[주석]폼네임명을 볼수있는 함수
'Call insert.ViewTbaleGride()	'[주석]테이블로 보기
'Call insert.ViewCreateTable()	'[주석]테이블쿼리
'Call insert.ViewFiledInsert
'Call insert.ViewFiledupdate
'=====================================================================
'+++++++[0]_End

'+++++++[2] 수정과 삭제의 경우만 활설화 된다. 입력(insert)때는 비활성화
'Call insert.setwhere(" intSeq = '"& request.QueryString("intSeq") &"' ")	'//수정과 삭제에서만 사용
'+++++++[2]_End

'// 기본으로 넘어오는 값 세팅
'Call insert.setDefaultData("")

'+++++++[3] 입력페이지 이외에는 [주석]처리한다.
'Call insert.setdataAdd("strCategory", request.QueryString(ProgramFolderName))
'+++++++[3]_End

Call insert.settablename(tablename)

Call DbOpen()

Call insert.setdataAdd("dencode", insert.getFormValue("dencode"))
Call insert.setdataAdd("denname", insert.getFormValue("denname"))
Call insert.setdataAdd("denmanager", insert.getFormValue("denmanager"))

Call insert.setdataAdd("hp1", insert.getFormValue("hp1"))
Call insert.setdataAdd("hp2", insert.getFormValue("hp2"))
Call insert.setdataAdd("hp3", insert.getFormValue("hp3"))

Call insert.setdataAdd("tel1", insert.getFormValue("tel1"))
Call insert.setdataAdd("tel2", insert.getFormValue("tel2"))
Call insert.setdataAdd("tel3", insert.getFormValue("tel3"))

Call insert.setdataAdd("fax1", insert.getFormValue("fax1"))
Call insert.setdataAdd("fax2", insert.getFormValue("fax2"))
Call insert.setdataAdd("fax3", insert.getFormValue("fax3"))

Call insert.setdataAdd("denpost", insert.getFormValue("denpost"))
Call insert.setdataAdd("denaddress", insert.getFormValue("denaddress"))
Call insert.setdataAdd("denaddress2", insert.getFormValue("denaddress2"))

Call insert.setdataAdd("message_set", insert.getFormValue("message_set"))
Call insert.setdataAdd("order_num", insert.getFormValue("order_num"))
Call insert.setdataAdd("memo", insert.getFormValue("memo"))
Call insert.setdataAdd("deninsertdate", FormatDateTime(now(),2) & " " & FormatDateTime(now(),4))

'+++++++[5] form에서 넘어오는 업로드될 파일 값 세팅 [함수명 바뀜insert-update-delete]
	'call insert.setFileData("file_1","insert")
	'Call insert.setinsertNumFileDataAdd("strImage") '//구번전 파일 업로드
'+++++++[5]_End

  dim strSql
  strSQL = insert.getqueryinsert

 'Response.write strSQL
 ' Response.end

  ' Response.end

  Call AdoConnExecute(strSQL)

Call DbClose()

set insert = nothing

'+++++++[6] 메세지를 바꿔주고 처리후 이동할 페이지를 정해준다.
Call jsAlertMsgUrl("등록되었습니다", "hospital_list.asp")
'Call jsAlertMsgUrl("등록되었습니다", "member_add.asp?"&GetString(""))
'+++++++[6]_End
%>