<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<meta charset="utf-8"/>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
call dbopen

Dim intSeq, page, sql, rs, dencode, denname, denmanager, hp1, hp2, hp3, tel1, tel2, tel3, fax1, fax2, fax3
Dim denpost, denaddress, denaddress2, message_set, order_num, memo, deninsertdate

intSeq = request.QueryString("intSeq")
page = request.QueryString("page")

sql = "SELECT * FROM denlist WHERE intSeq = '"&intSeq&"'"
set rs = dbconn.execute(sql)

dencode       = rs("dencode")
denname       = rs("denname")
denmanager    = rs("denmanager")
hp1           = rs("hp1")
hp2           = rs("hp2")
hp3           = rs("hp3")
tel1          = rs("tel1")
tel2          = rs("tel2")
tel3          = rs("tel3")
fax1          = rs("fax1")
fax2          = rs("fax2")
fax3          = rs("fax3")
denpost       = rs("denpost")
denaddress    = rs("denaddress")
denaddress2   = rs("denaddress2")
message_set   = rs("message_set")
order_num     = rs("order_num")
memo          = rs("memo")
deninsertdate = rs("deninsertdate")

rs.close
Set rs=Nothing
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<link rel="stylesheet" href="_css/sub.css" />
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
	$(document).ready(function() {

	});
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<script>
// 우편번호 찾기
function zipSearch(kind) {

	if(kind == undefined) kind = '';

	new daum.Postcode({

		oncomplete: function(data) {

			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 우편번호와 주소 정보를 해당 필드에 넣고, 커서를 상세주소 필드로 이동한다.

			eval('document.frmRequestForm.denpost').value = data.zonecode;

			if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우

					eval('document.frmRequestForm.denaddress').value = data.roadAddress;

                } else { // 사용자가 지번 주소를 선택했을 경우(J)

					eval('document.frmRequestForm.denaddress').value = data.jibunAddress;

                }

			//전체 주소에서 연결 번지 및 ()로 묶여 있는 부가정보를 제거하고자 할 경우,

			//아래와 같은 정규식을 사용해도 된다. 정규식은 개발자의 목적에 맞게 수정해서 사용 가능하다.

			//var addr = data.address.replace(/(\s|^)\(.+\)$|\S+~\S+/g, '');

			//document.getElementById('addr').value = addr;

			if(eval('document.frmRequestForm.'+kind+'denaddress2') != null)

				eval('document.frmRequestForm.'+kind+'denaddress2').focus();

		}
	}).open();
}
</script>
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">안과수정</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">안과관리</a></li>
		            <li><a href="#">안과수정</a></li>
				</ul>
			</div>
			<form class="form_m_add" name="frmRequestForm" method="post" onSubmit="return frmRequestForm_Submit(this);" action="hospital_edit_ok.asp?<%=getstring("")%>" enctype="multipart/form-data">
				<table class="table_write">
					<colgroup>
						<col style="width: 280px;">
						<col style="width: 488px;">
						<col style="width: 278px;">
						<col style="width: 488px;">
					</colgroup>
					<tbody>
						<tr>
							<th><em>*</em> 안과코드</th>
							<td>
							  <div class="phone_id"><%=dencode%></div>
							</td>
							<th>등록일</th>
							<td><%=Left(deninsertdate,4)%>.<%=mid(deninsertdate,6,2)%>.<%=mid(deninsertdate,9,2)%></td>
						</tr>
						<tr>
							<th><em>*</em> 안과명</th>
							<td colspan="3"><input type="text" name="denname" id="denname" maxlength="50" value="<%=denname%>" /></td>
						</tr>
						<tr>
							<th><em>*</em> 담당자</th>
							<td><input type="text" name="denmanager" id="denmanager" maxlength="50" value="<%=denmanager%>" /></td>
							<th>휴대폰번호</th>
							<td class="tel">
								<input type="text" name="hp1" maxlength="3" value="<%=hp1%>" />
								<i class="hyphen">-</i>
								<input type="text" name="hp2" maxlength="4" value="<%=hp2%>" />
								<i class="hyphen">-</i>
								<input type="text" name="hp3" maxlength="4" value="<%=hp3%>" />
							</td>
						</tr>
						<tr>
							<th><em>*</em> 전화번호</th>
							<td class="tel">
								<input type="text" name="tel1" id="tel1" maxlength="3" value="<%=tel1%>" />
								<i class="hyphen">-</i>
								<input type="text" name="tel2" id="tel2" maxlength="4" value="<%=tel2%>" />
								<i class="hyphen">-</i>
								<input type="text" name="tel3" id="tel3" maxlength="4" value="<%=tel3%>" />
							</td>
							<th>팩스번호</th>
							<td class="tel">
								<input type="text" name="fax1" maxlength="3" value="<%=fax1%>" />
								<i class="hyphen">-</i>
								<input type="text" name="fax2" maxlength="4" value="<%=fax2%>" />
								<i class="hyphen">-</i>
								<input type="text" name="fax3" maxlength="4" value="<%=fax3%>" />
							</td>
						</tr>
						<tr>
							<th><em>*</em> 주소</th>
							<td colspan="3" class="address">
								<div>
									<input type="text" name="denpost" maxlength="5" readonly value="<%=denpost%>" />
									<button type="button" class="btn_post" onClick="zipSearch()">우펀번호검색</button>
								</div>
								<input type="text" name="denaddress" maxlength="200" readonly value="<%=denaddress%>" />
								<input type="text" name="denaddress2" maxlength="100" value="<%=denaddress2%>" />
							</td>
						</tr>
						<tr>
							<th><em>*</em> 메시지설정</th>
							<td colspan="3" class="chk_wrap">
								<div class="chk_box">
									<input id="set_message01" type="radio" name="message_set" value="a" <%If message_set = "a" then%>checked<%End if%> />
									<label for="set_message01"><span></span><p>통합</p></label>
								</div>
								<div class="chk_box">
									<input id="set_message02" type="radio" name="message_set" value="b" <%If message_set = "b" then%>checked<%End if%> />
									<label for="set_message02"><span></span><p>제품카테고리별</p></label>
								</div>
							</td>
						</tr>
						<tr>
							<th><em>*</em> 목표주문건수</th>
							<td colspan="3">
								<input type="text" name="order_num" id="order_num" maxlength="5" value="<%=order_num%>" />
							</td>
						</tr>
						<tr>
							<th>관리자메모</th>
							<td colspan="3">
								<textarea class="memo" name="memo"><%=memo%></textarea>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="btns">
					<button type="button" class="btn_gray" onclick="location.href='hospital_list.asp';">목록</button>
					<button type="button" class="btn_blue" onClick="this.form.onsubmit();">저장</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<script language="javascript">
<!--
var ajaxObjects = new Array();
var RegexEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i; //이메일 요휴성검사
var RegexName = /^[가-힣a-zA-Z0-9]{2,10}$/; //이름 유효성 검사 2~10자 사이
var RegexId = /^[a-z0-9_\`\~\!\@\#\$\%\^\&\*\(\)\-\=\+\\\{\}\[\]\'\"\;\:\<\,\>\.\?\/]{4,16}$/; 
var RegexTel = /^[0-9]{8,12}$/; //전화번호 유효성 검사
var RegexPass	= /^[\S\x21-\x7E]{4,16}$/;  //비번 유효성 검사

function frmRequestForm_Submit(frm){

  if ( frm.denname.value == "" ) {
    alert("안과명을 입력하세요.");
	$("#denname").focus();
	return;
  }

  if ( frm.denmanager.value == "" ) {
    alert("담당자를 입력하세요.");
	$("#denmanager").focus();
	return;
  }

  if ( frm.tel1.value == "" ) {
    alert("전화번호를 입력하세요.");
	$("#tel1").focus();
	return;
  }

  if (f_is_alpha(frm.tel1) ){
	alert("전화번호는 숫자만으로 구성됩니다.");
	$("#tel1").focus();
	return;
  }

  if ( frm.tel2.value == "" ) {
    alert("전화번호를 입력하세요.");
	$("#tel2").focus();
	return;
  }

  if (f_is_alpha(frm.tel2) ){
	alert("전화번호는 숫자만으로 구성됩니다.");
	$("#tel2").focus();
	return;
  }

  if ( frm.tel3.value == "" ) {
    alert("전화번호를 입력하세요.");
	$("#tel3").focus();
	return;
  }

  if (f_is_alpha(frm.tel3) ){
	alert("전화번호는 숫자만으로 구성됩니다.");
	$("#tel3").focus();
	return;
  }

  if ( frm.denpost.value == "" ) {
    alert("주소를 입력하세요.");
	return;
  }

  if ( frm.denaddress.value == "" ) {
    alert("주소를 입력하세요.");
	return;
  }

  if ( frm.denaddress2.value == "" ) {
    alert("주소를 입력하세요.");
	return;
  }

  if ( frm.message_set[0].checked == false && frm.message_set[1].checked == false ) {
    alert("메시지설정을  체크하세요.");
	return;
  }

  if ( frm.order_num.value == "" ) {
    alert("목표주문건수를 입력하세요.");
	$("#order_num").focus();
	return;
  }

  if (f_is_alpha(frm.order_num) ){
	alert("목표주문건수는 숫자만으로 구성됩니다.");
	$("#order_num").focus();
	return;
  }

  //alert("ok");

  frm.submit();
}

function f_is_alpha( it ) {
  var alpha ='-';
  var numeric = '1234567890';
  var blank = ' ';
  var nonkorean = numeric;
  var i ;
  var t = it.value ;
  for ( i=0; i<t.length; i++ )
    if( nonkorean.indexOf(t.substring(i,i+1)) < 0) {
      break ;
    }
    if ( i != t.length ) {
      return true ;
    }
  return ;
}
//-->
</script>