<!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include file="_inc/head.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="_css/sub.css" />
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		$(document).ready(function() {
			// 카테고리 탭
			$("#search_date").datepicker({
				showOn: "both",
				// buttonImage: "images/sub/ic_cal.png",
				// buttonImageOnly: true,
				buttonText: "검색",
				dateFormat: "yy.mm.dd",
				monthNamesShort: ['01','02','03','04','05','06','07','08','09','10','11','12'],
				monthNames: ['.01','.02','.03','.04','.05','.06','.07','.08','.09','.10','.11','.12'],
				dayNames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
				showMonthAfterYear: true,
				showOtherMonths: true,
			});
			$('#search_date').datepicker('setDate', 'today');

			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function(){ 
				if(window.FileReader){
					var filename = $(this)[0].files[0].name;
				}
				else {
					var filename = $(this).val().split('/').pop().split('\\').pop(); 
				}
				$(this).siblings('.upload_name').val(filename);
			});

			function play() {
			  var audio = new Audio('media/sample_audio_ogg.ogg');
			  audio.play();
			}
		});
	</script>
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap" class="message">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="msgs_wrap">
				<div class="msgs_lpanel">
					<form>
						<div class="msgs_search">
							<input type="text" name="" placeholder="안과명, 안과코드, 이름" />
							<button type="button" class="btn_search">검색</button>
						</div>
						<ul class="dm_list">
							<li>
								<a href="#mags01">
									<div class="txt">
										<strong>강남 밝은눈 안과_FINEVISION</strong>
										<p>안녕하세요 저는 홍길동입니다. 어제 오후쯤에 제품을</p>
									</div>
									<div class="info">
										<span class="time">오전 6:30</span>
										<i class="ic_new">새로운 메시지</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags02">
									<div class="txt">
										<strong>아반안과_FINEVISION</strong>
										<p>음성메시지를 보냈습니다.</p>
									</div>
									<div class="info">
										<span class="time">오후 4:10</span>
										<i class="ic_new">새로운 메시지</i>
									</div>
								</a>
							</li>
							<li class="active">
								<a href="#mags02">
									<div class="txt">
										<strong>서울행복안과_통합</strong>
										<p>안녕하세요.</p>
									</div>
									<div class="info">
										<span class="time">오전 9:00</span>
										<i class="ic_new">새로운 메시지</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags03">
									<div class="txt">
										<strong>미래안과_통합</strong>
										<p>사진을 보냈습니다.</p>
									</div>
									<div class="info">
										<span class="time">어제</span>
										<i class="ic_notify_off">알림켜기</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags03">
									<div class="txt">
										<strong>바른눈안과_FINEVISION</strong>
										<p>네 감사합니다!</p>
									</div>
									<div class="info">
										<span class="time">03월 14일</span>
										<i class="ic_notify_off">알림켜기</i>
									</div>
								</a>
							</li>
						</ul>
					</form>
				</div>
				<div class="msgs_stage">
					<div class="msgs_top">
						<div class="date">
							<input id="search_date" type="text" name="" />
							<!-- <button type="button">검색</button> -->
						</div>
						<button type="button" class="btn_notify on">알림</button>
					</div>
					<div class="msgs_holder">
						<div class="msgs_con">
							<div class="msgs_list_wrap">
								<!-- 내 말풍선 -->
								<div class="msgs_balloon_wrap msgs_mine">
									<div class="right_txt">
										<span class="time">오전 8:40</span>
									</div>
									<div class="msgs_balloon">
										<p>네 감사합니다.</p>
									</div>
								</div>

								<!-- 날짜표시 -->
								<div class="system_msgs date_divider">
									<span>2020년 5월 12일</span>
								</div>

								<!-- 상대방 말풍선 -->
								<div class="msgs_balloon_wrap msgs_partner">
									<div class="msgs_profile">
										<div class="profile_img"><img src="images/sub/partner_profile.png"/></div>
										<strong class="partner_name">김친구</strong>
									</div>
									<div class="msgs_container">
										<div class="msgs_balloon">
											<p>안녕하세요 상품문의에 관련하여 질문이 있습니다. 안녕하세요 상품문의에 관련하여 질문이 있습니다. 안녕하세요 상품문의에 관련하여 질문이 있습니다.</p>
										</div>
										<div class="right_txt"></div>
									</div>
								</div>
								<div class="msgs_balloon_wrap msgs_partner">
									<div class="msgs_balloon">
										<p>음성 메시지로 주문 요청 드립니다.</p>
									</div>
									<div class="right_txt"></div>
								</div>
								<div class="msgs_balloon_wrap msgs_partner">
									<div class="msgs_balloon msgs_audio">
										<button type="button" class="btn_play" onclick="play()">음성 메시지 재생</button>
										<div>
											<img src="images/sub/audio_wave.svg"/>
											<span class="audio_time">1:00</span>
										</div>
									</div>
									<div class="right_txt">
										<span class="time">오전 8:40</span>
									</div>
								</div>

								<!-- 입장/퇴장 -->
								<div class="system_msgs in_out_divider">
									<span><em>김친구</em> 님이 나가셨습니다</span>
								</div>
								<div class="system_msgs in_out_divider">
									<span><em>홍길동</em> 님이 들어오셨습니다</span>
								</div>

								<div class="msgs_balloon_wrap msgs_partner">
									<div class="msgs_profile">
										<div class="profile_img"><img src="images/sub/partner_profile_noimg.svg"/></div>
										<strong class="partner_name">홍길동</strong>
									</div>
									<div class="msgs_balloon msgs_photo">
										<img src="images/sub/msgs_img.png"/>
									</div>
								</div>
								<div class="msgs_balloon_wrap msgs_partner">
									<div class="msgs_balloon">
										<p>안녕하세요</p>
									</div>
									<div class="right_txt">
										<span class="unread_count">2</span>
										<span class="time">오전 11:32</span>
									</div>
								</div>

								<div class="msgs_balloon_wrap msgs_mine">
									<div class="right_txt">
										<span class="unread_count">3</span>
										<span class="time">오전 8:40</span>
									</div>
									<div class="msgs_balloon">
										<p>네 안녕하세요!</p>
									</div>
								</div>
							</div>
							<form class="msgs_write_form">
								<div class="write_area">
									<textarea class="textarea"></textarea>
								</div>
								<div class="input_area">
									<div class="file_box">
										<input class="upload_name" disabled="disabled">
										<input type="file" id="write_file" name="file_1" class="upload_hidden">
										<label for="write_file"><i></i>사진첨부</label>
									</div>
									<button type="button" class="btn_send">전송</button>
								</div>
							</form>
						</div>
						<div class="msgs_info">
							<strong class="msgs_title">강남 밝은눈 안과<span>FINEVISION</span></strong>
							<span class="mem_total">참여자 2</span>
							<ul class="mem_list">
								<li>
									<div class="m_profile"><img src="images/sub/partner_profile_noimg.svg"/></div>
									<p class="m_name">아반</p>
									<button type="button" class="btn_modify">수정</button>
								</li>
								<li>
									<div class="m_profile"><img src="images/sub/partner_profile_noimg.svg"/></div>
									<p class="m_name">우전</p>
									<button type="button" class="btn_modify">수정</button>
								</li>
								<li>
									<div class="m_profile"><img src="images/sub/partner_profile_noimg.svg"/></div>
									<p class="m_name">홍길동</p>
									<button type="button" class="btn_modify">수정</button>
								</li>
								<li>
									<div class="m_profile"><img src="images/sub/partner_profile02.png"/></div>
									<p class="m_name">김철수</p>
									<button type="button" class="btn_modify">수정</button>
								</li>
							</ul>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
