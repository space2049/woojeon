<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>(주)우전메디칼</title>
<style>
body { font-size:15px; font-weight:600; color:#333; }
table { width:80%;   border-collapse:collapse;}
table td, table th { padding:10px; height:20px; border:1px solid #000; }
a { color:#333; text-decoration:none; }
</style>

</head>
<body>
    <p>우전메디칼 관리자 코딩 리스트</p>
    <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <colgroup>
            <col style="width: 360px;"/>
            <col style="width: *;"/>
            <col style="width: 90px;"/>
        </colgroup>
        <thead>
            <tr style="background-color: #d9d9d9">
                <th>1차메뉴</th>
                <th>2차메뉴</th>
                <th>URL</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="1">로그인</td>
                <td>-</td>
                <td><a href="index.asp" target="_blank">화면보기</a></td>
            </tr>

            <tr>
                <td rowspan="4">안과관리</td>
                <td>안과목록</td>
                <td><a href="hospital_list.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>안과등록</td>
                <td><a href="hospital_add.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>회원목록</td>
                <td><a href="member_list.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>회원등록(안과검색 팝업)</td>
                <td><a href="member_add.asp" target="_blank">화면보기</a></td>
            </tr>

            <tr>
                <td rowspan="3">제품관리</td>
                <td>제품목록</td>
                <td><a href="prd_list.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>제품등록</td>
                <td><a href="prd_add.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>카테고리관리</td>
                <td><a href="category.asp" target="_blank">화면보기</a></td>
            </tr>

            <tr>
                <td rowspan="2">우전메시지</td>
                <td>메시지화면 (메시지방 선택 이전 화면)</td>
                <td><a href="message.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>메시지화면 (메시지방 선택 이후 화면)</td>
                <td><a href="message02.asp" target="_blank">화면보기</a></td>
            </tr>

            <tr>
                <td rowspan="2">게시판관리</td>
                <td>공지사항관리</td>
                <td><a href="prd_list.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>공지사항등록</td>
                <td><a href="prd_add.asp" target="_blank">화면보기</a></td>
            </tr>

            <tr>
                <td rowspan="2">이벤트관리</td>
                <td>출석체크이벤트</td>
                <td><a href="attendance_event.asp" target="_blank">화면보기</a></td>
            </tr>
            <tr>
                <td>안과이벤트</td>
                <td><a href="order_event.asp" target="_blank">화면보기</a></td>
            </tr>

        </tbody>
    </table>
</body>
</html>