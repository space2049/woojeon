<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<meta charset="utf-8"/>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
call dbopen

Const UploadFolder = "/upload/"
Const tablename = "denlist"

Dim listfree, gum1, gum2
gum1 = requests("gum1")
gum2 = requests("gum2")
listfree = requests("listfree")

If listfree = "n" Then
  gum1 = ""
  gum2 = ""
End If

Dim mem_intSeq, mem_sql, mem_rs, strHp, strName, strbirthday1, strbirthday2, dencode, denname, intPermit, strmemo, file_1, mem_page, dtmInsertDate

mem_intSeq = request.QueryString("mem_intSeq")
mem_page = request.QueryString("mem_page")

mem_sql = "SELECT * FROM mtb_member2 WHERE intSeq = '"&mem_intSeq&"'"
set mem_rs = dbconn.execute(mem_sql)

strHp         = mem_rs("strHp")
strName       = mem_rs("strName")
strbirthday1  = mem_rs("strbirthday1")
strbirthday2  = mem_rs("strbirthday2")
dencode       = mem_rs("dencode")
denname       = mem_rs("denname")
intPermit     = mem_rs("intPermit")
strmemo       = mem_rs("strmemo")
file_1        = mem_rs("file_1")
dtmInsertDate = mem_rs("dtmInsertDate")

mem_rs.close
Set mem_rs=Nothing
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<link rel="stylesheet" href="_css/sub.css" />
<script>
$(document).ready(function() {
	// 첨부파일
	var fileTarget = $('.file_box .upload_hidden');
	fileTarget.on('change', function(){
		if(window.FileReader){
			var filename = $(this)[0].files[0].name;
		}
		else {
			var filename = $(this).val().split('/').pop().split('\\').pop();
		}
		$(this).siblings('.upload_name').val(filename);
	});


	// 안과검색 팝업 
	var posY;
	function bodyFreezeScroll() {
		posY = $(window).scrollTop();
		$('html').css('position','fixed');
		$('html').css("top",-posY);
	}

	function bodyUnfreezeScroll() {
		$('html').css('position','static');
		posY = $(window).scrollTop(posY);
	}

	$('.btn_srh_h').click(function(){
		$('.pop_srh_h').fadeIn();
		bodyFreezeScroll();
	});
	$('.pop_close, .popup .btns .btn_gray').click(function(){
		$('.popup').fadeOut();
		bodyUnfreezeScroll();
	});

});

function buttona(){
  $('.pop_srh_h').fadeIn();
  bodyFreezeScroll();
}

function frmRequestForm_Submit2(frm) {
  var frm = document.frm;
  if (frm.gum2.value == ""){ alert("검색어를 입력하세요."); frm.gum2.focus(); return;	}
  frm.submit();
}
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

</head>
<%if requests("listname") = "a" then%>
<body onload="javascript:buttona();">
<%else%>
<body>
<%end if%>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
		  <!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">회원수정</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">안과관리</a></li>
					<li><a href="#">회원수정</a></li>
				</ul>
			</div>
			<form class="form_m_add" name="frmRequestForm" method="post" onSubmit="return frmRequestForm_Submit(this);" action="member_edit_ok.asp?<%=getstring("")%>" enctype="multipart/form-data">
			<input type="hidden" name="page" />
			<input type="hidden" name="listfree" value="<%=listfree%>" />
            <input type="hidden" name="gum1" value="<%=gum1%>" />
			<input type="hidden" name="gum2" value="<%=gum2%>" />
			<input type="hidden" name="listname" value="a" />
				<div class="section">
					<div class="table_tit">
						<h3>회원정보</h3>
					</div>
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: 488px;">
							<col style="width: 280px;">
							<col style="width: 488px;">
						</colgroup>
						<tbody>
							<tr>
								<th><em>*</em> 휴대폰아이디</th>
								<td>
									<div class="phone_id">
										<input type="text" name="istrHpt" id="istrHpt" placeholder="'-' 없이 입력" maxlength="11" <%If requests("istrHpt") <> "" Then%>value="<%=requests("istrHpt")%>"<%else%>value="<%=strHp%>"<%End if%> />
										<button type="button" id="BtnIdchk1">중복확인</button>
										<input type="hidden" class="id_check2" name="id_check2" value="<%=requests("id_check2")%>">
									</div>
								</td>
								<th>등록일</th>
								<td><%=Left(dtmInsertDate,4)%>.<%=mid(dtmInsertDate,6,2)%>.<%=mid(dtmInsertDate,9,2)%></td>
							</tr>
							<tr>
								<th><em>*</em> 비밀번호</th>
								<td colspan="3" class="pw">
									<div>
										<input type="password" name="istrPwdt" id="istrPwdt" placeholder="비밀번호입력" value="<%=requests("istrPwdt")%>" />
										<input type="password" name="istrPwdt2" id="istrPwdt2" placeholder="비밀번호확인" value="<%=requests("istrPwdt2")%>" />
									</div>
									<p class="noti">최소 8자~16자 (영문 대문자, 소문자, 숫자, 특수문자 중 3가지 조합)<br/>비밀번호를 입력하지 않으시면, 기존 비밀번호와 동일하게 유지됩니다.</p>
								</td>
							</tr>
							<tr>
								<th><em>*</em> 이름</th>
								<td><input type="text" name="istrNamet" id="istrNamet" maxlength="30" <%If requests("istrNamet") <> "" Then%>value="<%=requests("istrNamet")%>"<%else%>value="<%=strName%>"<%End if%> /></td>
							</tr>
							<%
							Dim xx, yy
							%>
							<tr>
								<th><em>*</em> 생일</th>
								<td colspan="3" class="birthday">
									<select name="istrbirthdayt1">
									    <option value="">선택</option>
									    <%For xx=1 To 12 Step 1%>
										<option value="<%=xx%>" <%If requests("istrbirthdayt1") <> "" then%><%If CInt(requests("istrbirthdayt1")) = xx then%>selected<%End if%><%else%><%If CInt(strbirthday1) = xx then%>selected<%End if%><%End if%>><%=xx%>월</option>
										<%next%>
									</select>
									<select name="istrbirthdayt2">
									    <option value="">선택</option>
									    <%For yy=1 To 31 Step 1%>
										<option value="<%=yy%>" <%If requests("istrbirthdayt2") <> "" then%><%If CInt(requests("istrbirthdayt2")) = yy then%>selected<%End if%><%else%><%If CInt(strbirthday2) = yy then%>selected<%End if%><%End if%>><%=yy%>일</option>
										<%next%>
									</select>
								</td>
							</tr>
							<tr>
								<th>프로필이미지</th>
								<td>
									<div class="file_box">
										<input class="upload_name" disabled="disabled">
										<input type="file" id="write_file" name="file_1" class="upload_hidden">
										<label for="write_file">파일첨부</label>
									</div>
								</td>
								<td colspan="2"><%If Len(file_1) > 0 then%><%=Split(file_1,":")(1)%><%End if%></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="section">
					<div class="table_tit">
						<h3>회원설정</h3>
					</div>
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th><em>*</em> 안과</th>
								<td class="hospital">
									<button type="button" class="btn_srh_h">안과검색</button>
									<p><input type="text" name="dencode_view" style="border:none;background-color:#fafafa;height:30px;text-align:left;" value="<%=denname%> (<%=dencode%>)" /></p>
									<input type="hidden" name="dencode" value="<%=dencode%>" />
									<input type="hidden" name="denname" value="<%=denname%>" />
								</td>
							</tr>
							<tr>
								<th><em>*</em> 승인여부</th>
								<td>
									<select name="iintpermitt">
										<option value="s" <%If requests("iintpermitt") <> "" then%><%If requests("iintpermitt") = "s" then%>selected<%End if%><%else%><%If intpermit = "s" then%>selected<%End if%><%End if%>>승인</option>
										<option value="m" <%If requests("iintpermitt") <> "" then%><%If requests("iintpermitt") = "m" then%>selected<%End if%><%else%><%If intpermit = "m" then%>selected<%End if%><%End if%>>미승인</option>
										<option value="b" <%If requests("iintpermitt") <> "" then%><%If requests("iintpermitt") = "b" then%>selected<%End if%><%else%><%If intpermit = "b" then%>selected<%End if%><%End if%>>보류</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>관리자메모</th>
								<td>
									<textarea class="memo" name="istrmemot"><%If requests("iintpermitt") <> "" then%><%=requests("istrmemot")%><%else%><%=strmemo%><%End if%></textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="btns">
					<button type="button" class="btn_gray" onclick="location.href='member_list.asp';">목록</button>
					<button type="button" class="btn_blue" onClick="this.form.onsubmit();">저장</button>
				</div>
			</form>

            <%
			dim intTotalCount, intTotalPage

			dim intNowPage			: intNowPage 		= Request.QueryString("page")
			dim intPageSize			: intPageSize 		= 8
			dim intBlockPage		: intBlockPage 		= 8

			dim query_filde			: query_filde		= "*"
			dim query_Tablename		: query_Tablename	= TableName
			dim query_where			: query_where		= " intSeq > 0"
			dim query_orderby		: query_orderby		= "order by intseq desc"

			If gum1 <> "" And listfree <> "n" Then
				query_where = query_where &  " and "&gum1&" like '%"& gum2 &"%'"
			End If

			call intTotal

			dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

			dim sql, rs, rs3, count_, rs4, count2_
			sql = getQuery
			set rs = dbconn.execute(sql)

			set rs3 = dbconn.execute("SELECT COUNT('intSeq') as c FROM denlist")
			count_ = 0
			if  rs3.eof then
			Else
			  count_ = rs3("c")
			End If
			rs3.close()
			set rs3 = Nothing
			
			If gum1 <> "" And listfree <> "n" then
			set rs4 = dbconn.execute("SELECT COUNT('intSeq') as c FROM denlist where "&gum1&" like '%"& gum2 &"%'")
			count2_ = 0
			if  rs4.eof then
			Else
			  count2_ = rs4("c")
			End If
			rs4.close()
			set rs4 = Nothing
			End if

			'dbconn.execute("insert into Record_views (menu,manage_id,manage_name,ip,inputdate) values('회원리스트','"&session("userid")&"','"&session("username")&"','"&Request.ServerVariables("REMOTE_ADDR")&"','"&primaryval&"') ")
			%>

			<div class="popup pop_srh_h">
				<div class="pop_wrap">
					<h4 class="pop_tit">안과검색</h4>
					    <form name="frm" action="?<%=getstring("")%>" method="get" >
						<input type="hidden" name="listname" value="a" />
						<input type="hidden" name="istrHpt" value="<%=requests("istrHpt")%>" />
						<input type="hidden" name="id_check2" value="<%=requests("id_check2")%>" />
						<input type="hidden" name="istrPwdt" value="<%=requests("istrPwdt")%>" />
						<input type="hidden" name="istrPwdt2" value="<%=requests("istrPwdt2")%>" />
						<input type="hidden" name="istrNamet" value="<%=requests("istrNamet")%>" />
						<input type="hidden" name="istrbirthdayt1" value="<%=requests("istrbirthdayt1")%>" />
						<input type="hidden" name="istrbirthdayt2" value="<%=requests("istrbirthdayt2")%>" />
						<input type="hidden" name="iintpermitt" value="<%=requests("iintpermitt")%>" />
						<input type="hidden" name="istrmemot" value="<%=requests("istrmemot")%>" />
						<input type="hidden" name="mem_intSeq" value="<%=mem_intSeq%>" />
						<input type="hidden" name="mem_page" value="<%=mem_page%>" />
						<div class="pop_search">
							<select name="gum1">
								<option value="denname" <%If gum1 = "denname" And listfree <> "n" then%>selected<%End if%>>안과명</option>
								<option value="dencode" <%If gum1 = "dencode" And listfree <> "n" then%>selected<%End if%>>안과코드</option>
							</select>
							<input type="text" name="gum2" <%If listfree <> "n" then%>value="<%=gum2%>"<%End if%> />
							<button type="button" onclick="frmRequestForm_Submit2();">검색</button>
						</div>
						</form>
						<div class="board_num">
							<p><%If gum1 <> "" And listfree = "" Then%>검색 <%=count2_%> &nbsp;/ &nbsp;<%End if%>전체 <%=count_%></p>
						</div>
						<form name="dFrm" method="post">
						<table class="table_list">
							<colgroup>
								<col style="width: 60px;">
								<col style="width: 60px;">
								<col style="width: 140px;">
								<col style="width: 100px;">
								<col style="width: 260px;">
								<col style="width: 120px;">
							</colgroup>
							<thead>
								<th>선택</th>
								<th>번호</th>
								<th>안과명</th>
								<th>안과코드</th>
								<th>주소</th>
								<th>등록일</th>
							</thead>
							<tbody>
							      <%
									if not rs.eof then
									  rs.move MoveCount
									  Do while not rs.eof
								  %>
								<tr>
									<td>
										<div class="chk_box">
											<input id="mem_chk<%=intNowNum%>" type="radio" name="ChkCheck" value="<%=rs("dencode")%>|<%=rs("denname")%>|<%=rs("denname")%> (<%=rs("dencode")%>)" />
											<label for="mem_chk<%=intNowNum%>"><span></span></label>
										</div>
									</td>
									<td><%=intNowNum%></td>
									<td><%=rs("denname")%></td>
									<td><%=rs("dencode")%></td>
									<td><%=rs("denaddress")%></td>
									<td><%=Replace(Left(rs("deninsertdate"),10),"-",".")%></td>
								</tr>
								  <%
									  intNowNum = intNowNum - 1
									  rs.movenext
									  loop
									end if
										
									rs.close
									set rs = nothing
								  %>
							</tbody>
						</table>
						<%call Paging_list2("&listname=a")%>
						<!--<div class="list_paging">
							<a class="paging_prev" href="#">처음</a>
							<ul>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li class="active"><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">6</a></li>
								<li><a href="#">7</a></li>
							</ul>
							<a class="paging_next" href="#">맨끝</a>
						</div>-->
						<div class="btns">
							<button type="button" class="btn_gray" style="width:150px;">취소</button>
							<button type="button" class="btn_blue" style="width:150px;" onclick="del_page()">확인</button>
							<button type="button" class="btn_gray" onclick="frmRequestForm_Submit3('1','n')" style="width:150px;">목록</button>
						</div>
						<input type="hidden" name="ChkCheck" />
					    </form>
					<button type="button" class="pop_close">닫기</button>
				</div>
			</div>
			<%
			Call DbClose()
			%>





		</div>
	</div>
</div>
</body>
</html>
<script language="javascript">
<!--
var ajaxObjects = new Array();
var RegexEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i; //이메일 요휴성검사
var RegexName = /^[가-힣a-zA-Z0-9]{2,10}$/; //이름 유효성 검사 2~10자 사이
var RegexId = /^[a-z0-9_\`\~\!\@\#\$\%\^\&\*\(\)\-\=\+\\\{\}\[\]\'\"\;\:\<\,\>\.\?\/]{4,16}$/; 
var RegexTel = /^[0-9]{8,12}$/; //전화번호 유효성 검사
var RegexPass	= /^[\S\x21-\x7E]{4,16}$/;  //비번 유효성 검사

$(document).ready(function(){

	$("#BtnIdchk1").click(function(e){

      var frm = document.frmRequestForm;

	  if (frm.istrHpt.value == "<%=strHp%>") {
		alert("회원 본인 핸드폰번호입니다.");
		return;
	  }

      if (frm.istrHpt.value == ""){
	    alert("휴대폰번호를 입력해주세요.");
		$("#istrHpt").val('');
		$("#istrHpt").focus();
		return;
	  }

	  if(frm.istrHpt.value.length < 11){
	    alert("휴대폰번호를 정확히 입력해주세요.");
		$("#istrHpt").focus();
		return;
	  }

	  if (f_is_alpha(frm.istrHpt) ){
	    alert("휴대폰번호는 숫자만으로 구성됩니다.");
		$("#istrHpt").focus();
		return;
	  }

	  var id= $.trim($("#istrHpt").val())
	  var ajaxIndex = ajaxObjects.length;
	  ajaxObjects[ajaxIndex] = new sack();
	  ajaxObjects[ajaxIndex].method = "POST";
	  ajaxObjects[ajaxIndex].setVar("id", id);
	  ajaxObjects[ajaxIndex].requestFile = "/main/member_checking.asp";
	  ajaxObjects[ajaxIndex].onCompletion = function() { idchkValueComplete(ajaxIndex); } ;
	  ajaxObjects[ajaxIndex].runAJAX();
	});
})

function idchkValueComplete(index){

    var result=ajaxObjects[index].response
    if (result=="str_yes"){
	  alert("중복되는 휴대폰번호입니다.");
      $(".id_check2").val('1');
	  $("#istrHpt").val('');

    }else{

	  alert("사용가능한 휴대폰번호입니다.");
	  $(".id_check2").val('2');

    }
}

function frmRequestForm_Submit(frm){

  if (frm.istrHpt.value == "" ) {
    alert("휴대폰번호를 입력하세요.");
	$("#istrHpt").focus();
	return;
  }

  if ((frm.istrHpt.value != "<%=strHp%>") && frm.id_check2.value == "" ) {
    alert("휴대폰번호 중복확인을 해주세요.");
	return;
  }

  if ((frm.istrHpt.value != "<%=strHp%>") && frm.id_check2.value == "1" ) {
    alert("중복되는 휴대폰번호입니다. 중복확인을 다시 해주세요.");
	return;
  }

  if ( frm.istrPwdt.value != "" ) {
  
	  if ( frm.istrPwdt2.value == "" ) {
		alert("비밀번호 확인을 입력하세요.");
		$("#istrPwdt2").focus();
		return false;
	  }

	  var f1 = document.frmRequestForm;
	  var pw1 = f1.istrPwdt.value;
	  var pw2 = f1.istrPwdt2.value;
	  var pw_yn1 = 1;
	  var pw_yn2 = 1;
	  var pw_yn3 = 1;
	  var pw_yn4 = 1;

	  var m_Sp = /[\$\@\\\#\%\^\&\*\(\)\[\]\+\_\{\}\`\~\=\|\!]/;
	  var m_Sp2 = /[a-z]/g;
	  var m_Sp3 = /[0-9]/g;
	  var m_Sp4 = /[A-Z]/g;

	// alert("");

	  if(pw1.search(m_Sp) == -1) {
		pw_yn1 = 0;
	  }
		
	  if(pw1.search(m_Sp2) == -1) {
		pw_yn2 = 0;
	  }

	  if(pw1.search(m_Sp3) == -1) {
		pw_yn3 = 0;
	  }

	  if(pw1.search(m_Sp4) == -1) {
		pw_yn4 = 0;
	  }

	  var pw_yn_sum = pw_yn1 + pw_yn2 + pw_yn3 + pw_yn4;

	  if(pw_yn_sum < 3){
		alert("비밀번호를 8~16자의 대문자, 소문자, 숫자, 특수문자 중 3가지 이상의 조합으로 입력하세요."); 
		return;
	  }

	  if(pw1.length < 8 || pw1.length > 16){
		alert("비밀번호를 8~16자의 대문자, 소문자, 숫자, 특수문자 중 3가지 이상의 조합으로 입력하세요."); 
		return;
	  }

	  if( frm.istrPwdt.value != frm.istrPwdt2.value ) {
		alert("비밀번호가 다릅니다.");
		return;
	  }

  }

  if ( frm.istrNamet.value == "" ) {
    alert("이름을 입력하세요.");
	$("#istrNamet").focus();
	return;
  }

  if ( frm.istrbirthdayt1.value == "" ) {
    alert("생일을 선택하세요.");
	return;
  }

  if ( frm.istrbirthdayt2.value == "" ) {
    alert("생일을 선택하세요.");
	return;
  }

  if ( frm.dencode.value == "" ) {
    alert("안과를 선택하세요.");
	return;
  }

  //alert("ok");

  frm.submit();
}

function frmRequestForm_Submit3(a,b){
  document.frmRequestForm.page.value=a;
  document.frmRequestForm.listfree.value=b;
  document.frmRequestForm.action="member_edit.asp";
  document.frmRequestForm.method="get";
  document.frmRequestForm.submit();
}

function f_is_alpha( it ) {
  var alpha ='-';
  var numeric = '1234567890';
  var blank = ' ';
  var nonkorean = numeric;
  var i ;
  var t = it.value ;
  for ( i=0; i<t.length; i++ )
    if( nonkorean.indexOf(t.substring(i,i+1)) < 0) {
      break ;
    }
    if ( i != t.length ) {
      return true ;
    }
  return ;
}

function del_page(){

  var objForm = document.dFrm;
  var len = objForm.ChkCheck.length;
  var poll = "0";
  var poll1 = "";

  for(i = 0; i < len; i++) {

    if(objForm.ChkCheck[i].checked) {
      poll=objForm.ChkCheck[i].value;
	  poll1 = poll;
	}
  }

  if (poll=="0"){
    alert("안과 목록에 체크하세요.");
	return;
  }

  var poll2 = poll1.split("|");

  document.frmRequestForm.dencode_view.value = poll2[2];
  document.frmRequestForm.dencode.value = poll2[0];
  document.frmRequestForm.denname.value = poll2[1];

  $('.popup').fadeOut();
  bodyUnfreezeScroll2();
}

function bodyUnfreezeScroll2() {
	$('html').css('position','static');
	posY = $(window).scrollTop(posY);
}
//-->
</script> 