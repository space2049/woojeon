<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Const UploadFolder = "/upload/notice"
Const tablename = "orderevent"

dim year1 	        : year1 	          = RequestS("year1")
dim month1 	        : month1 	          = RequestS("month1")

If year1 = "" Then
  year1 = year(date)
End If

If month1 = "" Then
  month1 = month(date)
  If Len(month1) = 1 Then
    month1 = "0" & month1
  End if
End if

dim intTotalCount, intTotalPage

dim intNowPage			: intNowPage 		= Request.QueryString("page")
dim intPageSize			: intPageSize 		= 10
dim intBlockPage		: intBlockPage 		= 10

dim query_filde			: query_filde		= " at.den_code, at.yearmonth, at.counting, bt.denname, bt.denaddress, bt.denaddress2, bt.denmanager, bt.tel1, bt.tel2, bt.tel3, bt.hp1, bt.hp2, bt.hp3, bt.fax1, bt.fax2, bt.fax3, bt.order_num "
dim query_Tablename		: query_Tablename	= TableName & " as at inner join denlist as bt on at.den_code=bt.dencode "
dim query_where			: query_where		= " at.counting >= bt.order_num "
dim query_orderby		: query_orderby		= "order by at.intseq desc"

Dim yearmonth

yearmonth = year1 & month1

query_where = query_where &" and at.yearmonth = '"& yearmonth &"'"

call intTotal

dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

dim sql, rs
sql = getQuery
call dbopen
set rs = dbconn.execute(sql)
%> 
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
<link rel="stylesheet" href="_css/sub.css" />
<script>
$(document).ready(function() {

});
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">주문이벤트</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">이벤트관리</a></li>
					<li><a href="#">주문이벤트</a></li>
				</ul>
			</div>
			<form class="hospital_form" method="post" action="?">
				<div class="board_num">
					<select name="year1" onchange="this.form.submit();">
					    <%
						  Dim s
						  For s=2020 To year(date)
						%>
						<option value="<%=s%>" <%If s = CInt(year1) then%>selected<%End if%>><%=s%></option>
						<%next%>
					</select>
					<select name="month1" onchange="this.form.submit();">
					    <%
						  Dim t, tt
						  For t=1 To 12
						    If Len(t) = 1 Then
						      tt = "0" & t
							Else
							  tt = t
							End if
						%>
						<option value="<%=tt%>" <%If Cstr(tt) = Cstr(month1) then%>selected<%End if%>><%=tt%></option>
						<%next%>
					</select>
					<div class="right_btns">
						<button type="button" class="btn_add" onclick="location.href='order_excel.asp?year1=<%=year1%>&month1=<%=month1%>'">엑셀다운로드</button>
					</div>
				</div>
				<table class="table_list">
					<colgroup>
						<col style="width: 60px;">
						<col style="width: 100px;">
						<col style="width: 160px;">
						<col style="width: 120px;">
						<col style="width: 180px;">
						<col style="width: 140px;">
						<col style="width: 140px;">
						<col style="width: *;">
						<col style="width: 120px;">
						<col style="width: 120px;">
					</colgroup>
					<thead>
						<th>번호</th>
						<th>안과코드</th>
						<th>안과명</th>
						<th>담당자명</th>
						<th>담당자 휴대폰번호</th>
						<th>전화번호</th>
						<th>팩스</th>
						<th>주소</th>
						<th>목표주문건수</th>
						<th>실제주문건수</th>
					</thead>
					<tbody>
					    <%
						if not rs.eof Then

						  rs.move MoveCount
						  Do while not rs.eof
					    %>
						<tr>
							<td><%=intNowNum%></td>
							<td><%=rs("den_code")%></td>
							<td><%=rs("denname")%></td>
							<td><%=rs("denmanager")%></td>
							<td><%=rs("hp1")%>-<%=rs("hp2")%>-<%=rs("hp3")%></td>
							<td><%=rs("tel1")%>-<%=rs("tel2")%>-<%=rs("tel3")%></td>
							<td><%=rs("fax1")%>-<%=rs("fax2")%>-<%=rs("fax3")%></td>
							<td><%=rs("denaddress")%>&nbsp;<%=rs("denaddress2")%></td>
							<td><%=rs("order_num")%>건</td>
							<td><%=rs("counting")%>건</td>
						</tr>
						<%
						  intNowNum = intNowNum - 1
						  rs.movenext
						  Loop

						Else
					    %>
					    <tr>
						  <td colspan="10">목록이 없습니다.</td>
					    </tr>
					    <%
						end if

						rs.close
						set rs = nothing
					    %>
					</tbody>
				</table>
				<%call Paging_list("")%>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<%
Call DbClose()
%>