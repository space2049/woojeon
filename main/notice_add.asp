<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<meta charset="utf-8"/>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include file="_inc/head.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
	<link rel="stylesheet" href="_css/sub.css" />
	<script>
		$(document).ready(function() {
			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function(){ 
				if(window.FileReader){
					var filename = $(this)[0].files[0].name;
				}
				else {
					var filename = $(this).val().split('/').pop().split('\\').pop(); 
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});
	</script>
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">공지사항관리</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">게시판관리</a></li>
		            <li><a href="#">공지사항관리</a></li>
				</ul>
			</div>
			<form class="form_notice" name="frmRequestForm" method="post" onSubmit="return frmRequestForm_Submit(this);" action="notice_add_ok.asp?<%=getstring("")%>" enctype="multipart/form-data">
				<div class="section">
					<div class="table_tit">
						<h3>기본정보</h3>
					</div>
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: 488px;">
							<col style="width: 280px;">
							<col style="width: 488px;">
						</colgroup>
						<tbody>
							<tr>
								<th>작성자</th>
								<td>관리자</td>
								<th>등록일</th>
								<td>YYYY-MM-DD 00:00:00</td>
							</tr>
							<tr>
								<th>조회수</th>
								<td colspan="3">1233</td>
							</tr>
							<tr>
								<th>URL</th>
								<td colspan="3">https://www.avansoft.co.kr/notice01</td>
							</tr>
							<tr>
								<th><em>*</em> 제목</th>
								<td colspan="3"><input type="text" name="wtitle" id="wtitle"></td>
							</tr>
							<tr>
								<th><em>*</em> 내용</th>
								<td colspan="3"><textarea class="notice_con" name="wcontents" id="wcontents"></textarea></td>
							</tr>
							<tr>
								<th>이미지첨부</th>
								<td colspan="3">
									<div class="file_box">
										<input class="upload_name" disabled="disabled">
										<input type="file" id="write_file" name="file_1" class="upload_hidden">
										<label for="write_file">파일첨부</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="btns">
						<button type="button" class="btn_gray" onclick="location.href='notice.asp';">목록</button>
						<button type="button" class="btn_blue" onClick="this.form.onsubmit();">저장</button>
					</div>
				</div>
				<div class="section push_section">
					<div class="table_tit">
						<h3>푸시알림관리</h3>
					</div>
					<table class="table_list">
						<colgroup>
							<col style="width: 300px;">
							<col style="width: *;">
							<col style="width: 300px;">
						</colgroup>
						<thead>
							<tr>
								<th>발송일시</th>
								<th>푸시메시지</th>
								<th>발송자</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>YYYY-MM-DD 00:00:00</td>
								<td>출석체크 이벤트 당첨자 안내!</td>
								<td>관리자(id)</td>
							</tr>
						</tbody>
					</table>
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th>푸시메시지</th>
								<td>
									<div class="push">
										<input type="text" name=""/>
										<button type="button">발송</button>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<script language="javascript">
<!--
function frmRequestForm_Submit(frm){

  if ( frm.wtitle.value == "" ) {
    alert("제목을 입력하세요.");
	$("#wtitle").focus();
	return;
  }

  if ( frm.wcontents.value == "" ) {
    alert("내용을 입력하세요.");
	$("#wcontents").focus();
	return;
  }

  frm.submit();
}

function f_is_alpha( it ) {
  var alpha ='-';
  var numeric = '1234567890';
  var blank = ' ';
  var nonkorean = numeric;
  var i ;
  var t = it.value ;
  for ( i=0; i<t.length; i++ )
    if( nonkorean.indexOf(t.substring(i,i+1)) < 0) {
      break ;
    }
    if ( i != t.length ) {
      return true ;
    }
  return ;
}
//-->
</script>