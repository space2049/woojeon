<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Const UploadFolder = "/upload/"
Const tablename = "mtb_member2"

dim gum1 	        : gum1 	          = RequestS("gum1")
dim gum2 	        : gum2 	          = RequestS("gum2")
Dim intPermit_serch : intPermit_serch = RequestS("intPermit_serch")
dim dtmInsertDate1  : dtmInsertDate1  = RequestS("dtmInsertDate1")
dim dtmInsertDate2  : dtmInsertDate2  = RequestS("dtmInsertDate2")

dim intTotalCount, intTotalPage, dtmInsertDate11, dtmInsertDate22

dim intNowPage			: intNowPage 		= Request.QueryString("page")
dim intPageSize			: intPageSize 		= 10
dim intBlockPage		: intBlockPage 		= 10

dim query_filde			: query_filde		= "*"
dim query_Tablename		: query_Tablename	= TableName
dim query_where			: query_where		= " intSeq > 0 and intGubun <> 9 "
dim query_orderby		: query_orderby		= "order by intGubun desc, intseq desc"

if Len(gum2) > 0 then
	query_where = query_where &" and "& gum1 & " like '%" & gum2 & "%'"
end If

if intPermit_serch <> "" then
	query_where = query_where &" and intPermit = '"& intPermit_serch &"'"
end If

If dtmInsertDate1 <> "" and dtmInsertDate2 <> "" Then
    dtmInsertDate11 = dtmInsertDate1 & " " & "00:00:00"
	dtmInsertDate22 = dtmInsertDate2 & " " & "23:59:59"
	query_where = query_where &  " and dtmInsertDate between '" & dtmInsertDate11 & "' and '"&dtmInsertDate22&"'"
End If

call intTotal

dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

dim sql, rs, rs3, count_, rs4, count2_
sql = getQuery
call dbopen
set rs = dbconn.execute(sql)

set rs3 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where intSeq > 0 and intGubun <> 9")
count_ = 0
if  rs3.eof then
Else
  count_ = rs3("c")
End If
rs3.close()
set rs3 = Nothing

If gum2 <> "" Or intPermit_serch <> "" Or (dtmInsertDate1 <> "" and dtmInsertDate2 <> "") then
  set rs4 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where "& query_where &"")
  count2_ = 0
  if  rs4.eof then
  Else
    count2_ = rs4("c")
  End If
  rs4.close()
  set rs4 = Nothing
End If

'dbconn.execute("insert into Record_views (menu,manage_id,manage_name,ip,inputdate) values('회원리스트','"&session("userid")&"','"&session("username")&"','"&Request.ServerVariables("REMOTE_ADDR")&"','"&primaryval&"') ")
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->

<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
<link rel="stylesheet" href="_css/sub.css" />
<script>
/*
$(document).ready(function() {
	$( "#period_start" ).datepicker({
		showOn: "button",
		buttonImage: "images/sub/ic_cal_g.svg",
		buttonImageOnly: true,
		buttonText: "날짜 선택",
		dateFormat: "yy.mm.dd"
	});
	$( "#period_end" ).datepicker({
		showOn: "button",
		buttonImage: "images/sub/ic_cal_g.svg",
		buttonImageOnly: true,
		buttonText: "날짜 선택",
		dateFormat: "yy.mm.dd"
	});
});
*/
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

<link rel="stylesheet" href="/avanplus/_master/js/jquery-ui-1.11.4.custom/jquery-ui.css" media="screen" title="no title">
<script src="/avanplus/_master/js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script>
$(document).ready(function(){

	$('#datepicker').datepicker({
		inline: true,
		showOtherMonths: true,
		showMonthAfterYear: true,
		monthNames: [ '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12' ],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		dateFormat: 'yy-mm-dd',
	});

	$('#datepicker2').datepicker({
		inline: true,
		showOtherMonths: true,
		showMonthAfterYear: true,
		monthNames: [ '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12' ],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		dateFormat: 'yy-mm-dd',
	});
});

function frmRequestForm_Submit2(frm) {
  var frm = document.frm;
  if (frm.gum2.value == "" && frm.intPermit_serch[1].checked == false && frm.intPermit_serch[2].checked == false && frm.intPermit_serch[3].checked == false && (frm.dtmInsertDate1.value == "" || frm.dtmInsertDate2.value == "")){ alert("검색어, 승인여부, 등록일 중 하나 이상을 입력하거나 체크하세요."); frm.gum2.focus(); return;	}
  frm.submit();
}

function member_delete(a) {

  sub = confirm("삭제하시겠습니까.")
  if (sub == false) {
    return;
  }

  document.fmList_b.intSeq.value = a;
  document.fmList_b.action = "member_list_delete.asp";
  document.fmList_b.submit();

}
</script>

<form name="fmList_b" method="get">
<input type="hidden" name="intSeq" />
<input type="hidden" name="page" value="<%=Request.QueryString("page")%>" />
</form>

</head>
<body>
<div id="A_Wrap">
  <div id="A_Header">
	<!-- #include file="_inc/header.asp" -->
  </div>
  <div id="A_Container_Wrap">
	<div id="A_Container_L">
	  <!-- #include file="_inc/gnb.asp" -->
	</div>
	<div id="A_Container_C">
	  <div class="tit_area">
		<h2 class="sub_tit">회원목록</h2>
		<ul class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">안과관리</a></li>
		  <li><a href="#">회원목록</a></li>
		</ul>
	  </div>
	  <form class="member_form" name="frm" action="?<%=getstring("")%>" method="get" >
	  <div class="search_area">
		<table class="table_write">
		  <colgroup>
			<col style="width: 280px;">
			<col style="width: *;">
		  </colgroup>
		  <tbody>
			<tr>
			  <th>검색어</th>
			  <td class="s_word">
			    <select name="gum1">
				  <option value="strHp" <%If gum1 = "stgrHp" then%>selected<%End if%>>휴대폰아이디</option>
				  <option value="strName" <%If gum1 = "strName" then%>selected<%End if%>>이름</option>
				</select>
				<input type="text" name="gum2" value="<%=gum2%>">
			  </td>
			</tr>
			<tr>
			  <th>승인여부</th>
			  <td class="chk_wrap">
				<div class="chk_box">
				  <input id="approval_all" type="radio" name="intPermit_serch" checked value="" />
				  <label for="approval_all"><span></span><p>전체</p></label>
				</div>
				<div class="chk_box">
				  <input id="approval01" type="radio" name="intPermit_serch" value="s" <%If intPermit_serch = "s" then%>checked<%End if%> />
				  <label for="approval01"><span></span><p>승인</p></label>
				</div>
				<div class="chk_box">
				  <input id="approval02" type="radio" name="intPermit_serch" value="m" <%If intPermit_serch = "m" then%>checked<%End if%> />
				  <label for="approval02"><span></span><p>미승인</p></label>
				</div>
				<div class="chk_box">
				  <input id="approval03" type="radio" name="intPermit_serch" value="b" <%If intPermit_serch = "b" then%>checked<%End if%> />
				  <label for="approval03"><span></span><p>보류</p></label>
				</div>
			  </td>
			</tr>
			<tr>
			  <th>등록일</th>
			  <td class="period">
				<div><input id="datepicker" type="date" name="dtmInsertDate1" placeholder="YYYY-MM-DD" value="<%=dtmInsertDate1%>" readonly></div>
				<div><input id="datepicker2" type="date" name="dtmInsertDate2" placeholder="YYYY-MM-DD" value="<%=dtmInsertDate2%>" readonly></div>
			  </td>
			</tr>
		  </tbody>
	    </table>
	    <div class="btns">
		  <button type="button" class="btn_blue" onclick="frmRequestForm_Submit2();">검색</button>
	    </div>
      </div>
	  </form>
      <div class="board_num">
	    <p><%If gum2 <> "" Or intPermit_serch <> "" Or (dtmInsertDate1 <> "" and dtmInsertDate2 <> "") then%>검색 <%=count2_%> / <%End if%>전체 <%=count_%></p>
	    <div class="right_btns">
		  <button type="button" class="btn_add" onclick="location.href='member_add.asp'">등록</button>
	    </div>
      </div>
	  <form name="dFrm" method="post">
	  <table class="table_list">
		<colgroup>
		  <col style="width: 70px;">
		  <col style="width: 100px;">
		  <col style="width: 100px;">
		  <col style="width: 200px;">
		  <col style="width: 120px;">
		  <col style="width: 150px;">
		  <col style="width: 120px;">
		  <col style="width: 180px;">
		  <col style="width: 90px;">
		  <col style="width: 140px;">
		  <col style="width: 200px;">
		</colgroup>
		<thead>
		  <th>
			<div class="chk_box">
			  <input type="checkbox" name="all_chk" id="all_chk" value=0 onclick="CheckAll();">
			  <label for="all_chk"><span></span></label>
			</div>
		  </th>
		  <th>번호</th>
		  <th>회원코드</th>
		  <th>휴대폰아이디</th>
		  <th>이름</th>
		  <th>생일</th>
		  <th>안과코드</th>
		  <th>안과명</th>
		  <th>승인여부</th>
		  <th>등록일</th>
		  <th>관리</th>
		</thead>
		<tbody>
		  <%
		    Dim intSeq, strName, strJumin1, strJumin2, strId, strPwd, strZip, strAddr1, strAddr2,strActionMessage,intAction
	        Dim strPhone, strMobil, strEmail, strJob, dtmInsertDate, strMemo, intGubun, service_confirm, intPermit

			if not rs.eof Then

			  rs.move MoveCount
			  Do while not rs.eof
				
			  intseq	= rs("intseq")
			  strId 	= rs("strId")
			  strName 	= rs("strName")
			  strPwd 	= rs("strPwd")
			  intAction = rs("intAction")
			  intGubun 	= rs("intGubun")
			  strEmail 	= rs("strEmail")
			  intPermit = rs("intPermit")

			  if Len(intAction) = 0 or isnull(intAction) or intAction = "0" then
				intAction = "정상"
			  elseif intAction = "1" then
				intAction = "<font color=#FF9900>강퇴</font>"
			  elseif intAction = "2" then
				intAction = "<font color=red>탈퇴</font>"
			  elseif intAction = "3" then
				intAction = "<font color=red>임시제한</font>"
			  end If

			  if intPermit = "s" then
				intPermit = "승인"
			  elseif intPermit = "b" then
				intPermit = "보류"
			  elseif intPermit = "m" then
				intPermit = "미승인"
			  end If
	      %>
		  <tr>
			<td>
			  <div class="chk_box">
				<input type="checkbox" id="chk_<%=intNowNum%>" name="ChkCheck" value="<%=rs("intSeq")%>" />
			    <label for="chk_<%=intNowNum%>"><span></span></label>
			  </div>
			</td>
			<td><%=intNowNum%></td>
			<td><%=strId%></td>
			<td><%=rs("strHp")%></td>
			<td><%=rs("strName")%></td>
			<td><%=rs("strbirthday1")%>월 <%=rs("strbirthday2")%>일</td>
			<td><%=rs("dencode")%></td>
			<td><%=rs("denname")%></td>
			<td><%=intPermit%></td>
			<td><%=Left(rs("dtmInsertDate"),10)%></td>
			<td>
			  <button type="button" class="btn_s btn_del" onclick="javascript:member_delete('<%=rs("intSeq")%>');">삭제</button>
			  <button type="button" class="btn_s btn_modify" onclick="location.href='member_edit.asp?mem_intSeq=<%=rs("intSeq")%>&mem_page=<%=Request.QueryString("page")%>';">수정</button>
			</td>
		  </tr>
		  <%
			  intNowNum = intNowNum - 1
			  rs.movenext
			  Loop

			Else
		  %>
		  <tr>
		    <td colspan="11">목록이 없습니다.</td>
		  </tr>
		  <%
			end if
				
			rs.close
			set rs = nothing
	      %>
		</tbody>
	  </table>
	  <div class="table_batch">
		<p>선택한 회원을</p>
		<select name="intPermit">
		  <option value="m">미승인</option>
		  <option value="s">승인</option>
		  <option value="b">보류</option>
		</select>
		<button type="button" onclick="del_page()">일괄처리</button>
	  </div>
	  <input type="hidden" name="ChkCheck" />
	  </form>
	  <%call Paging_list("")%>
	  <!--<div class="list_paging">
		<a class="paging_prev" href="#">처음</a>
		<ul>
		  <li><a href="#">1</a></li>
		  <li><a href="#">2</a></li>
		  <li><a href="#">3</a></li>
		  <li class="active"><a href="#">4</a></li>
		  <li><a href="#">5</a></li>
		  <li><a href="#">6</a></li>
		  <li><a href="#">7</a></li>
		</ul>
		<a class="paging_next" href="#">맨끝</a>
	  </div>-->
      </form>
	</div>
  </div>
</div>
</body>
</html>
<%
Call DbClose()
%>

<script>
function del_page(){

  var objForm = document.dFrm;
  var len = objForm.ChkCheck.length
  var poll = "0"
  var poll1 = ""
  var poll2 = ""
  var intpermit = objForm.intPermit.value;

  for(i = 0; i < len; i++) {

    if(objForm.ChkCheck[i].checked) {
      poll=objForm.ChkCheck[i].value;
	  poll2 = poll + ",";
	  poll1 = poll1 + poll2;
	}
  }

  if (poll=="0"){
    alert("일괄처리할 목록에 체크하세요.");
	return;
  }

  location.href="member_list_change.asp?sms_no="+poll1+"&page=<%=Request.QueryString("page")%>&intpermit="+intpermit;
}

var flag = true;
function CheckAll(checkFlag){

  for(i = 0; i < document.dFrm.elements.length; ++i){

	if(document.dFrm.elements[i].name == 'ChkCheck'){
	  if (document.dFrm.elements[i].value == 'on' )
		document.dFrm.elements[i].checked = false ;
	  else
		document.dFrm.elements[i].checked = flag  ;
	}
  }
  if(flag == true){
	flag = false; // document.frmSearch.btnAll.value = "전체해제";
  }
  else{
	flag = true;  // document.frmSearch.btnAll.value = "전체선택";
  }
}

</script>