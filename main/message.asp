<!DOCTYPE html>
<html lang="ko">
<head>
	<!-- #include file="_inc/head.asp" -->
	<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="_css/sub.css" />
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		$(document).ready(function() {
			// 카테고리 탭
			// $(".msgs_contents").hide();
			// $(".mags_list li:first").addClass("active").show();
			// $(".msgs_contents:first").show();
			// $(".mags_list li").click(function() {
			// 	$(".mags_list li").removeClass("active");
			// 	$(".mags_list li").removeClass("bd_none");
			// 	$(this).prev().addClass('bd_none');
			// 	$(this).addClass("active");
			// 	$(".msgs_contents").hide();
			// 	var activeTab = $(this).find("a").attr("href");
			// 	$(activeTab).fadeIn();
			// 	return false;
			// });

			// 첨부파일
			var fileTarget = $('.file_box .upload_hidden');
			fileTarget.on('change', function(){ 
				if(window.FileReader){
					var filename = $(this)[0].files[0].name;
				}
				else {
					var filename = $(this).val().split('/').pop().split('\\').pop(); 
				}
				$(this).siblings('.upload_name').val(filename);
			});
		});
	</script>
	<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap" class="message">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="msgs_wrap">
				<div class="msgs_lpanel">
					<form>
						<div class="msgs_search">
							<input type="text" name="" placeholder="안과명, 안과코드, 이름" />
							<button type="button" class="btn_search">검색</button>
						</div>
						<ul class="dm_list">
							<li>
								<a href="#mags01">
									<div class="txt">
										<strong>강남 밝은눈 안과_FINEVISION</strong>
										<p>안녕하세요 저는 홍길동입니다. 어제 오후쯤에 제품을</p>
									</div>
									<div class="info">
										<span class="time">오전 6:30</span>
										<i class="ic_new">새로운 메시지</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags02">
									<div class="txt">
										<strong>아반안과_FINEVISION</strong>
										<p>음성메시지를 보냈습니다.</p>
									</div>
									<div class="info">
										<span class="time">오후 4:10</span>
										<i class="ic_new">새로운 메시지</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags02">
									<div class="txt">
										<strong>서울행복안과_통합</strong>
										<p>안녕하세요.</p>
									</div>
									<div class="info">
										<span class="time">오전 9:00</span>
										<i class="ic_new">새로운 메시지</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags03">
									<div class="txt">
										<strong>미래안과_통합</strong>
										<p>사진을 보냈습니다.</p>
									</div>
									<div class="info">
										<span class="time">어제</span>
										<i class="ic_notify_off">알림켜기</i>
									</div>
								</a>
							</li>
							<li>
								<a href="#mags03">
									<div class="txt">
										<strong>바른눈안과_FINEVISION</strong>
										<p>네 감사합니다!</p>
									</div>
									<div class="info">
										<span class="time">03월 14일</span>
										<i class="ic_notify_off">알림켜기</i>
									</div>
								</a>
							</li>
						</ul>
					</form>
				</div>
				<div class="msgs_stage">
					<div class="msgs_top"></div>
					<div class="msgs_holder">
						<!-- 대화 내역이 없을 떄 no_his 클래스 추가 -->
						<div class="msgs_con no_his">
							<strong>대화 내역이 없습니다.</strong>
						</div>
						<div class="msgs_info"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
