<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>

<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->

<!--#include virtual = "/json2/json2.asp"-->
<%
Function BytesToStr(bytes)
  Dim Stream
  Set Stream = Server.CreateObject("Adodb.Stream")
      Stream.Type = 1
	  Stream.Open
	  Stream.Write bytes
	  Stream.Position = 0
	  Stream.Type = 2
	  Stream.Charset = "utf-8"
	  BytesToStr = Stream.ReadText
	  Stream.Close
  Set Stream = Nothing
End Function

If Request.TotalBytes > 0 Then
  Dim IngBytesCount, jsonText
  IngBytesCount = Request.TotalBytes
  jsonText = BytesToStr(Request.BinaryRead(IngBytesCount))
  Response.ContentType = "text/plain"
'  Response.write "Your " & Request.ServerVariables("REQUEST_METHOD") & " data was : " & jsonText
End If

Dim json_value

dim Info : set Info = JSON.parse(jsonText)
json_value = JSON.stringify(Info, null, 2) 
set Info = Nothing

dim Info2 : set Info2 = JSON.parse(join(array(  json_value  )))

session.codepage = 65001
Response.ContentType = "application/json"
Response.Charset = "utf-8"

Dim jwt, jose, success, claims

set jwt = Server.CreateObject("Chilkat_9_5_0.Jwt")

set jose = Server.CreateObject("Chilkat_9_5_0.JsonObject")

success = jose.AppendString("alg","HS256")
success = jose.AppendString("typ","JWT")

set claims = Server.CreateObject("Chilkat_9_5_0.JsonObject")

'success = claims.AppendString("iss","http://example.org")
'success = claims.AppendString("sub","John")
'success = claims.AppendString("aud","http://example.com")
'success = claims.AppendString("test","01011112222")

Dim strId, strPwd, intSeq, strName, strJumin1, strJumin2, strZip, strAddr1, strAddr2,strActionMessage,intAction
Dim strPhone, strMobil, strEmail, strJob, dtmInsertDate, strMemo, intGubun, success_yn, message_name

Dim strSQL,arrData,intLoop
Const UploadFolder = "/upload/"
Const tablename = "mtb_member2"

strId = Info2.memhp
strPwd = Info2.mempw

Dim strPass

Call DbOpen()

success_yn = ""
message_name = ""

strSQL = "select strPwd,strName,strId,intgubun from "&tablename&" where strHp = '"&strId&"' and intaction = 0"
arrData = getAdoRsArray(strSQL)

If not isArray(arrData)  Then

  success = claims.AppendString("login","No")
  success_yn = "false"
  message_name = strId

Else

	strPass = arrData(0,0)
	strName = arrData(1,0)
	strId	= arrData(2,0)
	intgubun= arrData(3,0)

	If strPass <> Encrypt_Sha(strPwd) Then

	    success = claims.AppendString("login","No")
		success_yn = "false"
		message_name = strPwd

	Else

		dbconn.execute("update mtb_member2 set strlogin = '"&FormatDateTime(now(),2) & " " & FormatDateTime(now(),4)&"' where strid = '"&strId&"'")

		success = claims.AppendString("Login","Yes")
		success_yn = "true"
		message_name = ""

	End If
End If

Call DbClose()


Dim curDateTime, strJwt

curDateTime = jwt.GenNumericDate(0)
success = claims.AddIntAt(-1,"Iat",curDateTime)

success = claims.AddIntAt(-1,"Nbf",curDateTime)

success = claims.AddIntAt(-1,"Exp",curDateTime + 36000)

jwt.AutoCompact = 1

strJwt = jwt.CreateJwt(jose.Emit(),claims.Emit(),"secret")

If success_yn = "false" Then
  strJwt = ""
  message_name = "사용자 아이디 및 비밀번호가 일치하지 않습니다."
End if

Response.Write "{""success"":"&success_yn&", ""token"":"""&Server.HTMLEncode( strJwt)&""", ""message"":"""&message_name&"""}"



'Dim token, sigVerified, leeway, bTimeValid, payload, json2, joseHeader

'token = Server.HTMLEncode( strJwt)

'sigVerified = jwt.VerifyJwt(token,"secret")
'Response.Write "<pre>" & Server.HTMLEncode( "with correct password: " & sigVerified) & "</pre>"

'sigVerified = jwt.VerifyJwt(token,"secret2")
'Response.Write "<pre>" & Server.HTMLEncode( "with incorrect password " & sigVerified) & "</pre>"

'leeway = 60
'bTimeValid = jwt.IsTimeValid(token,leeway)
'Response.Write "<pre>" & Server.HTMLEncode( "time constraints valid: " & bTimeValid) & "</pre>"

'payload = jwt.GetPayload(token)

'Response.Write "<pre>" & Server.HTMLEncode( payload) & "</pre>"

'set json2 = Server.CreateObject("Chilkat_9_5_0.JsonObject")
'success = json2.Load(payload)
'json2.EmitCompact = 0
'Response.Write "<pre>" & Server.HTMLEncode( json2.Emit()) & "</pre>"

'joseHeader = jwt.GetHeader(token)

'Response.Write "<pre>" & Server.HTMLEncode( joseHeader) & "</pre>"

'success = json2.Load(joseHeader)
'json2.EmitCompact = 0
'Response.Write "<pre>" & Server.HTMLEncode( json2.Emit()) & "</pre>"
%>