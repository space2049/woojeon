<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Const UploadFolder = "/upload/notice"
Const tablename = "wnotice"

dim gum1 	        : gum1 	          = RequestS("gum1")
dim gum2 	        : gum2 	          = RequestS("gum2")

dim intTotalCount, intTotalPage

dim intNowPage			: intNowPage 		= Request.QueryString("page")
dim intPageSize			: intPageSize 		= 10
dim intBlockPage		: intBlockPage 		= 10

dim query_filde			: query_filde		= "*"
dim query_Tablename		: query_Tablename	= TableName
dim query_where			: query_where		= " intSeq > 0 "
dim query_orderby		: query_orderby		= "order by intseq desc"

if Len(gum2) > 0 then
	query_where = query_where &" and "& gum1 & " like '%" & gum2 & "%'"
end If

call intTotal

dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

dim sql, rs, rs3, count_, rs4, count2_
sql = getQuery
call dbopen
set rs = dbconn.execute(sql)

set rs3 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where intSeq > 0")
count_ = 0
if  rs3.eof then
Else
  count_ = rs3("c")
End If
rs3.close()
set rs3 = Nothing

If gum2 <> "" then
  set rs4 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where "& query_where &"")
  count2_ = 0
  if  rs4.eof then
  Else
    count2_ = rs4("c")
  End If
  rs4.close()
  set rs4 = Nothing
End If
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<link rel="stylesheet" href="_css/sub.css" />
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

<script>
function frmRequestForm_Submit2(frm) {
  var frm = document.frm;
  if (frm.gum2.value == ""){ alert("검색어를 입력하세요."); frm.gum2.focus(); return;	}
  frm.submit();
}

function notice_delete(a) {

  sub = confirm("삭제하시겠습니까.")
  if (sub == false) {
    return;
  }

  document.fmList_b.intSeq.value = a;
  document.fmList_b.action = "notice_list_delete.asp";
  document.fmList_b.submit();

}
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

<form name="fmList_b" method="get">
<input type="hidden" name="intSeq" />
<input type="hidden" name="page" value="<%=Request.QueryString("page")%>" />
</form>

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">공지사항관리</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">게시판관리</a></li>
		            <li><a href="#">공지사항관리</a></li>
				</ul>
			</div>
			    <form class="hospital_form" name="frm" action="?<%=getstring("")%>" method="get" >
				<div class="search_area">
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th>검색어</th>
								<td class="s_word">
									<select name="gum1">
										<option value="wtitle" <%If gum1 = "wtitle" then%>selected<%End if%>>제목</option>
										<option value="wcontents" <%If gum1 = "wcontents" then%>selected<%End if%>>내용</option>
										<option value="writer" <%If gum1 = "writer" then%>selected<%End if%>>작성자</option>
									</select>
									<input type="text" name="gum2" id="gum2" value="<%=gum2%>" />
								</td>
							</tr>
						</tbody>
					</table>
					<div class="btns">
						<button type="button" class="btn_blue" onclick="frmRequestForm_Submit2();">검색</button>
					</div>
				</div>
			    </form>
				<div class="board_num">
					<p><%If gum2 <> "" then%>검색 <%=count2_%> / <%End if%>전체 <%=count_%></p>
					<div class="right_btns">
						<button type="button" class="btn_add" onclick="location.href='notice_add.asp'">등록</button>
					</div>
				</div>
				<form name="dFrm" method="post">
				<table class="table_list">
					<colgroup>
						<col style="width: 100px;">
						<col style="width: 120px;">
						<col style="width: *;">
						<col style="width: 180px;">
						<col style="width: 150px;">
						<col style="width: 260px;">
					</colgroup>
					<thead>
						<th>
						  <div class="chk_box">
						    <input type="checkbox" name="all_chk" id="all_chk" value=0 onclick="CheckAll();">
						    <label for="all_chk"><span></span></label>
						  </div>
					    </th>
						<th>번호</th>
						<th>제목</th>
						<th>작성자</th>
						<th>등록일</th>
						<th>관리</th>
					</thead>
					<tbody>
					    <%
						if not rs.eof Then

						  rs.move MoveCount
						  Do while not rs.eof
					    %>
						<tr>
							<td>
							  <div class="chk_box">
								<input type="checkbox" id="chk_<%=intNowNum%>" name="ChkCheck" value="<%=rs("intSeq")%>" />
								<label for="chk_<%=intNowNum%>"><span></span></label>
							  </div>
							</td>
							<td><%=intNowNum%></td>
							<td class="tit"><%=rs("wtitle")%></td>
							<td><%=rs("writer")%></td>
							<td><%=Left(rs("insertdate"),10)%></td>
							<td>
								<button type="button" class="btn_s btn_del" onclick="javascript:notice_delete('<%=rs("intSeq")%>');">삭제</button>
								<button type="button" class="btn_s btn_modify" onclick="location.href='notice_edit.asp?intSeq=<%=rs("intSeq")%>&page=<%=Request.QueryString("page")%>';">수정</button>
							</td>
						</tr>
						<%
						  intNowNum = intNowNum - 1
						  rs.movenext
						  Loop

						Else
					    %>
					    <tr>
						  <td colspan="6">목록이 없습니다.</td>
					    </tr>
					    <%
						end if

						rs.close
						set rs = nothing
					    %>
					</tbody>
				</table>
				<div class="left_btns">
					<button type="button" class="btn_del" onclick="del_page()">선택삭제</button>
				</div>
				<input type="hidden" name="ChkCheck" />
			    </form>
			    <%call Paging_list("")%>
		</div>
	</div>
</div>
</body>
</html>
<%
Call DbClose()
%>

<script>
function del_page(){

  var objForm = document.dFrm;
  var len = objForm.ChkCheck.length
  var poll = "0"
  var poll1 = ""
  var poll2 = ""

  for(i = 0; i < len; i++) {

    if(objForm.ChkCheck[i].checked) {
      poll=objForm.ChkCheck[i].value;
	  poll2 = poll + ",";
	  poll1 = poll1 + poll2;
	}
  }

  if (poll=="0"){
    alert("삭제할 목록에 체크하세요.");
	return;
  }

  location.href="notice_list_change.asp?sms_no="+poll1+"&page=<%=Request.QueryString("page")%>";
}

var flag = true;
function CheckAll(checkFlag){

  for(i = 0; i < document.dFrm.elements.length; ++i){

	if(document.dFrm.elements[i].name == 'ChkCheck'){
	  if (document.dFrm.elements[i].value == 'on' )
		document.dFrm.elements[i].checked = false ;
	  else
		document.dFrm.elements[i].checked = flag  ;
	}
  }
  if(flag == true){
	flag = false; // document.frmSearch.btnAll.value = "전체해제";
  }
  else{
	flag = true;  // document.frmSearch.btnAll.value = "전체선택";
  }
}

</script>