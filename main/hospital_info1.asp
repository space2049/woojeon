<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/json2/json2.asp"-->
<%
Function BytesToStr(bytes)
  Dim Stream
  Set Stream = Server.CreateObject("Adodb.Stream")
      Stream.Type = 1
	  Stream.Open
	  Stream.Write bytes
	  Stream.Position = 0
	  Stream.Type = 2
	  Stream.Charset = "utf-8"
	  BytesToStr = Stream.ReadText
	  Stream.Close
  Set Stream = Nothing
End Function

' 1) 제이슨 형식 콜이 있는지 확인
If Request.TotalBytes > 0 Then

  Dim IngBytesCount, jsonText
  IngBytesCount = Request.TotalBytes
  jsonText = BytesToStr(Request.BinaryRead(IngBytesCount))
  Response.ContentType = "text/plain"
  'Response.write "Your " & Request.ServerVariables("REQUEST_METHOD") & " data was : " & jsonText

  Dim json_value, denname

  dim Info : set Info = JSON.parse(jsonText)
  json_value = JSON.stringify(Info, null, 2) 
  set Info = Nothing

  dim Info2 : set Info2 = JSON.parse(join(array(  json_value  )))

  denname = Info2.denname

End If

session.codepage = 65001
Response.ContentType = "application/json"
Response.Charset = "utf-8"

Dim jwt, jose, success, claims

set jwt = Server.CreateObject("Chilkat_9_5_0.Jwt")
set jose = Server.CreateObject("Chilkat_9_5_0.JsonObject")

success = jose.AppendString("alg","HS256")
success = jose.AppendString("typ","JWT")


set claims = Server.CreateObject("Chilkat_9_5_0.JsonObject")

'success = claims.AppendString("iss","http://example.org")
'success = claims.AppendString("sub","John")
'success = claims.AppendString("aud","http://example.com")
'success = claims.AppendString("test","01011112222")

Const UploadFolder = "/upload/"
Const tablename = "denlist"

Call DbOpen()

dim intTotalCount, intTotalPage

Dim intNowPage          : intNowNum         = 1
dim intPageSize			: intPageSize 		= 1000
dim intBlockPage		: intBlockPage 		= 10

dim query_filde			: query_filde		= "*"
dim query_Tablename		: query_Tablename	= TableName
dim query_where			: query_where		= " intSeq > 0 "
dim query_orderby		: query_orderby		= "order by intseq desc"

if Len(denname) > 0 then
	query_where = query_where &" and denname like '%" & denname & "%'"
end If

call intTotal

dim intNowNum : intNowNum = intTotalCount

dim sql, rs, rs3, count_, rs4, count2_

sql = getQuery

set rs = dbconn.execute(sql)

set rs3 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where intSeq > 0")
count_ = 0
if  rs3.eof then
Else
  count_ = rs3("c")
End If
rs3.close()
set rs3 = Nothing

If denname <> "" then
  set rs4 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where "& query_where &"")
  count2_ = 0
  if  rs4.eof then
  Else
    count2_ = rs4("c")
  End If
  rs4.close()
  set rs4 = Nothing
End If

Dim success_yn, message_name

if not rs.eof Then

  rs.move MoveCount
  Do while not rs.eof
    
    success = claims.AppendString("num",intNowNum)
	success = claims.AppendString("dencode",rs("dencode"))
	success = claims.AppendString("denname",rs("denname"))
	success = claims.AppendString("denaddress",rs("denaddress"))
	success = claims.AppendString("denaddress2",rs("denaddress2"))

  intNowNum = intNowNum - 1
  rs.movenext
  Loop

  success_yn = "true"
  message_name = ""

Else

  success_yn = "false"
  message_name = "검색 목록이 없습니다."

end If

rs.close
set rs = Nothing

Call DbClose()

Dim curDateTime, strJwt

curDateTime = jwt.GenNumericDate(0)
success = claims.AddIntAt(-1,"Iat",curDateTime)

success = claims.AddIntAt(-1,"Nbf",curDateTime)

success = claims.AddIntAt(-1,"Exp",curDateTime + 36000)

jwt.AutoCompact = 1

strJwt = jwt.CreateJwt(jose.Emit(),claims.Emit(),"secret")

If success_yn = "false" Then
  strJwt = ""
End if

Response.Write "{""success"":"&success_yn&", ""token"":"""&Server.HTMLEncode( strJwt)&""", ""message"":"""&message_name&"""}"




'Dim token, sigVerified, leeway, bTimeValid, payload, json2, joseHeader

'token = Server.HTMLEncode( strJwt)

'sigVerified = jwt.VerifyJwt(token,"secret")
'Response.Write "<pre>" & Server.HTMLEncode( "with correct password: " & sigVerified) & "</pre>"

'sigVerified = jwt.VerifyJwt(token,"secret2")
'Response.Write "<pre>" & Server.HTMLEncode( "with incorrect password " & sigVerified) & "</pre>"

'leeway = 60
'bTimeValid = jwt.IsTimeValid(token,leeway)
'Response.Write "<pre>" & Server.HTMLEncode( "time constraints valid: " & bTimeValid) & "</pre>"

'payload = jwt.GetPayload(token)

'Response.Write "<pre>" & Server.HTMLEncode( payload) & "</pre>"

'set json2 = Server.CreateObject("Chilkat_9_5_0.JsonObject")
'success = json2.Load(payload)
'json2.EmitCompact = 0
'Response.Write "<pre>" & Server.HTMLEncode( json2.Emit()) & "</pre>"

'joseHeader = jwt.GetHeader(token)

'Response.Write "<pre>" & Server.HTMLEncode( joseHeader) & "</pre>"

'success = json2.Load(joseHeader)
'json2.EmitCompact = 0
'Response.Write "<pre>" & Server.HTMLEncode( json2.Emit()) & "</pre>"
%>