<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
<link rel="stylesheet" href="_css/main.css" />
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->
<script>
//$(document).ready(function() {

//		$('.loginForm li').click(function(){
//			$(this).find('label').hide();
//		});
//});

function login(){
	if ( document.frmRequestForm.strId.value.replace(/ /gi, "") == "" ) { alert("아이디를 입력해주세요"); document.frmRequestForm.strId.focus(); return; }
	if ( document.frmRequestForm.strPwd.value.replace(/ /gi, "") == "" ) { alert("비밀번호를 입력해주세요"); document.frmRequestForm.strPwd.focus(); return; }
	document.frmRequestForm.submit();
}

function setFocus()
{
  if ( document.forms[0].strId.value == "" )
    document.forms[0].strId.focus();
  else
    document.forms[0].strPwd.focus();
}
</script>
</head>
<body onLoad="setFocus();">
<div id="A_Wrap">
	<div id="A_Container_Wrap" class="login_wrap">
		<form class="login_form" name="frmRequestForm" id="frmRequestForm" method="post" action="loginOk.asp" onKeydown="javascript:if(event.keyCode == 13) login();" >
			<h2>ADMIN LOGIN</h2>
			<p>웹 사이트 운영을 위한 관리자 모드입니다.<br/>아이디와 패스워드를 입력하신 후 로그인해 주시기 바랍니다.</p>
			<input type="text" name="strId" placeholder="USER ID" />
			<input type="password" name="strPwd" placeholder="PASSWORD" />
			<button type="button" onclick="login();">로그인</button>
		</form>
	</div>
</div>
</body>
</html>