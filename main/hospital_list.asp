<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Const UploadFolder = "/upload/"
Const tablename = "denlist"

dim gum1 	        : gum1 	          = RequestS("gum1")
dim gum2 	        : gum2 	          = RequestS("gum2")
dim dtmInsertDate1  : dtmInsertDate1  = RequestS("dtmInsertDate1")
dim dtmInsertDate2  : dtmInsertDate2  = RequestS("dtmInsertDate2")

dim intTotalCount, intTotalPage, dtmInsertDate11, dtmInsertDate22

dim intNowPage			: intNowPage 		= Request.QueryString("page")
dim intPageSize			: intPageSize 		= 10
dim intBlockPage		: intBlockPage 		= 10

dim query_filde			: query_filde		= "*"
dim query_Tablename		: query_Tablename	= TableName
dim query_where			: query_where		= " intSeq > 0 "
dim query_orderby		: query_orderby		= "order by intseq desc"

if Len(gum2) > 0 then
	query_where = query_where &" and "& gum1 & " like '%" & gum2 & "%'"
end If

If dtmInsertDate1 <> "" and dtmInsertDate2 <> "" Then
    dtmInsertDate11 = dtmInsertDate1 & " " & "00:00:00"
	dtmInsertDate22 = dtmInsertDate2 & " " & "23:59:59"
	query_where = query_where &  " and deninsertdate between '" & dtmInsertDate11 & "' and '"&dtmInsertDate22&"'"
End If

call intTotal

dim intNowNum : intNowNum = intTotalCount - (intPageSize * (intNowPage-1))

dim sql, rs, rs3, count_, rs4, count2_
sql = getQuery
call dbopen
set rs = dbconn.execute(sql)

set rs3 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where intSeq > 0")
count_ = 0
if  rs3.eof then
Else
  count_ = rs3("c")
End If
rs3.close()
set rs3 = Nothing

If gum2 <> "" Or (dtmInsertDate1 <> "" and dtmInsertDate2 <> "") then
  set rs4 = dbconn.execute("SELECT COUNT('intSeq') as c FROM "& tablename &" where "& query_where &"")
  count2_ = 0
  if  rs4.eof then
  Else
    count2_ = rs4("c")
  End If
  rs4.close()
  set rs4 = Nothing
End If

'dbconn.execute("insert into Record_views (menu,manage_id,manage_name,ip,inputdate) values('회원리스트','"&session("userid")&"','"&session("username")&"','"&Request.ServerVariables("REMOTE_ADDR")&"','"&primaryval&"') ")
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
<link rel="stylesheet" href="_css/sub.css" />
<script>
/*
$(document).ready(function() {
	$( "#period_start" ).datepicker({
		showOn: "button",
		buttonImage: "images/sub/ic_cal_g.svg",
		buttonImageOnly: true,
		buttonText: "날짜 선택",
		dateFormat: "yy.mm.dd"
	});
	$( "#period_end" ).datepicker({
		showOn: "button",
		buttonImage: "images/sub/ic_cal_g.svg",
		buttonImageOnly: true,
		buttonText: "날짜 선택",
		dateFormat: "yy.mm.dd"
	});
});
*/
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

<link rel="stylesheet" href="/avanplus/_master/js/jquery-ui-1.11.4.custom/jquery-ui.css" media="screen" title="no title">
<script src="/avanplus/_master/js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script>
$(document).ready(function(){

	$('#datepicker').datepicker({
		inline: true,
		showOtherMonths: true,
		showMonthAfterYear: true,
		monthNames: [ '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12' ],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		dateFormat: 'yy-mm-dd',
	});

	$('#datepicker2').datepicker({
		inline: true,
		showOtherMonths: true,
		showMonthAfterYear: true,
		monthNames: [ '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12' ],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		dateFormat: 'yy-mm-dd',
	});
});

function frmRequestForm_Submit2(frm) {
  var frm = document.frm;
  if (frm.gum2.value == "" && (frm.dtmInsertDate1.value == "" || frm.dtmInsertDate2.value == "")){ alert("검색어, 등록일 중 하나 이상을 입력하세요."); frm.gum2.focus(); return;	}
  frm.submit();
}

function hospital_delete(a) {

  sub = confirm("삭제하시겠습니까.")
  if (sub == false) {
    return;
  }

  document.fmList_b.intSeq.value = a;
  document.fmList_b.action = "hospital_list_delete.asp";
  document.fmList_b.submit();

}
</script>

<form name="fmList_b" method="get">
<input type="hidden" name="intSeq" />
<input type="hidden" name="page" value="<%=Request.QueryString("page")%>" />
</form>

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">안과목록</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">안과관리</a></li>
		            <li><a href="#">안과목록</a></li>
				</ul>
			</div>
			    <form class="hospital_form" name="frm" action="?<%=getstring("")%>" method="get" >
				<div class="search_area">
					<table class="table_write">
						<colgroup>
							<col style="width: 280px;">
							<col style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th>검색어</th>
								<td class="s_word">
									<select name="gum1">
										<option value="denname" <%If gum1 = "denname" then%>selected<%End if%>>안과명</option>
										<option value="dencode" <%If gum1 = "dencode" then%>selected<%End if%>>안과코드</option>
									</select>
									<input type="text" name="gum2">
								</td>
							</tr>
							<tr>
								<th>등록일</th>
								<td class="period">
									<div><input id="datepicker" type="date" name="dtmInsertDate1" placeholder="YYYY-MM-DD" value="<%=dtmInsertDate1%>" readonly></div>
				                    <div><input id="datepicker2" type="date" name="dtmInsertDate2" placeholder="YYYY-MM-DD" value="<%=dtmInsertDate2%>" readonly></div>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="btns">
						<button type="button" class="btn_blue" onclick="frmRequestForm_Submit2();">검색</button>
					</div>
				</div>
				</form>
				<div class="board_num">
					<p><%If gum2 <> "" Or (dtmInsertDate1 <> "" and dtmInsertDate2 <> "") then%>검색 <%=count2_%> / <%End if%>전체 <%=count_%></p>
					<div class="right_btns">
						<button type="button" class="btn_add" onclick="location.href='hospital_add.asp'">등록</button>
					</div>
				</div>
				<form name="dFrm" method="post">
				<table class="table_list">
					<colgroup>
						<col style="width: 100px;">
						<col style="width: 180px;">
						<col style="width: 200px;">
						<col style="width: 200px;">
						<col style="width: 256px;">
						<col style="width: 120px;">
						<col style="width: 120px;">
						<col style="width: 140px;">
						<col style="width: 200px;">
					</colgroup>
					<thead>
						<th>번호</th>
						<th>안과코드</th>
						<th>안과명</th>
						<th>전화번호</th>
						<th>주소</th>
						<th>메시지설정</th>
						<th>회원수</th>
						<th>등록일</th>
						<th>관리</th>
					</thead>
					<tbody>
					    <%
						Dim sql2, rs2, dencode_count
						if not rs.eof Then

						  rs.move MoveCount
						  Do while not rs.eof

						    sql2 = "SELECT count(intSeq) FROM mtb_member2 WHERE dencode = '"&rs("dencode")&"'"
                            set rs2 = dbconn.execute(sql2)

							  dencode_count = rs2(0)

							rs2.close
							Set rs2=nothing
					    %>
						<tr>
							<td><%=intNowNum%></td>
							<td><%=rs("dencode")%></td>
							<td><%=rs("denname")%></td>
							<td><%=rs("tel1")%>-<%=rs("tel2")%>-<%=rs("tel3")%></td>
							<td><%=rs("denaddress")%>&nbsp;<%=rs("denaddress2")%></td>
							<td><%If rs("message_set") = "a" then%>통합<%ElseIf rs("message_set") = "b" then%>제품카테고리별<%End if%></td>
							<td><%=dencode_count%></td>
							<td><%=Left(rs("deninsertdate"),10)%></td>
							<td>
								<button type="button" class="btn_s btn_del" onclick="javascript:hospital_delete('<%=rs("intSeq")%>');">삭제</button>
			                    <button type="button" class="btn_s btn_modify" onclick="location.href='hospital_edit.asp?intSeq=<%=rs("intSeq")%>&page=<%=Request.QueryString("page")%>';">수정</button>
							</td>
						</tr>
						<%
						  intNowNum = intNowNum - 1
						  rs.movenext
						  Loop

						Else
					    %>
					    <tr>
						  <td colspan="9">목록이 없습니다.</td>
					    </tr>
					    <%
						end if

						rs.close
						set rs = nothing
					    %>
					</tbody>
				</table>
				</form>
				<%call Paging_list("")%>
		</div>
	</div>
</div>
</body>
</html>
<%
Call DbClose()
%>