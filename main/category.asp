<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!DOCTYPE html>
<html lang="ko">
<head>
<!-- #include file="_inc/head.asp" -->
<!-- s: 현재페이지에만 적용할 특정 스크립트 추가영역 -->	
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="_css/sub.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
	// 카테고리 탭
	$(".cate_contents").hide();
	$(".cate_tab li:first").addClass("active").show();
	$(".cate_contents:first").show();
	$(".cate_tab li").click(function() {
		$(".cate_tab li").removeClass("active");
		$(".cate_tab li").removeClass("bd_none");
		$(this).prev().addClass('bd_none');
		$(this).addClass("active");
		$(".cate_contents").hide();
		var activeTab = $(this).find("a").attr("href");
		$(activeTab).fadeIn();
		return false;
	});

	// 첨부파일
	var fileTarget = $('.file_box .upload_hidden');
	fileTarget.on('change', function(){ 
		if(window.FileReader){
			var filename = $(this)[0].files[0].name;
		}
		else {
			var filename = $(this).val().split('/').pop().split('\\').pop(); 
		}
		$(this).siblings('.upload_name').val(filename);
	});
});
</script>
<!-- e: 현재페이지에만 적용할 특정 스크립트 추가영역 -->

<script>
function category_delete(a) {

  sub = confirm("삭제하시겠습니까.")
  if (sub == false) {
    return;
  }

  document.fmList_b.intSeq.value = a;
  document.fmList_b.action = "category_delete.asp";
  document.fmList_b.submit();

}
</script>

<form name="fmList_b" method="get">
<input type="hidden" name="intSeq" />
</form>

</head>
<body>
<div id="A_Wrap">
	<div id="A_Header">
		<!-- #include file="_inc/header.asp" -->
	</div>
	<div id="A_Container_Wrap">
		<div id="A_Container_L">
			<!-- #include file="_inc/gnb.asp" -->
		</div>
		<div id="A_Container_C">
			<div class="tit_area">
				<h2 class="sub_tit">카테고리관리</h2>
				<ul class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">제품관리</a></li>
					<li><a href="#">카테고리관리</a></li>
				</ul>
			</div>
			<form class="category_form" name="frmRequestForm" method="post" onSubmit="return frmRequestForm_Submit(this);" action="hospital_add_ok.asp?<%=getstring("")%>" enctype="multipart/form-data">
				<div class="cate_wrap">
					<ul class="cate_tab">
					    <%
						Dim intSeq2
						If Request.QueryString("intSeq") = "" Then
						  intSeq2 = 0
						Else
						  intSeq2 = Request.QueryString("intSeq")
						End If
						
						Const tablename = "goodscategory"

						Dim rs
						call dbopen

						set rs = dbconn.execute("SELECT * FROM "& tablename &" order by intSeq asc")

					    if not rs.eof Then

						  Do while not rs.eof
						%>
						<li><a href="category.asp?intSeq=<%=rs("intSeq")%>" <%If CInt(intSeq2) = CInt(rs("intSeq")) then%>class="clickon"<%End if%>><%=rs("cate_name")%></a></li>
						<%
						  rs.movenext
						  Loop

						Else

						End If
						rs.close
						set rs = Nothing
						%>
						<li><a href="category.asp?intSeq=0" <%If intSeq2 = "0" then%>class="clickon"<%End if%>>카테고리추가</a></li>
					</ul>
					<%
					Dim cate_name, file_1, state_yn

					set rs = dbconn.execute("SELECT * FROM "& tablename &" where intSeq = "&intSeq2&"")

					if not rs.eof Then

					  cate_name = rs("cate_name")
					  If IsNull(rs("file_1")) Or rs("file_1") = "" Then
					    file_1 = ""
					  else
					    file_1 = Split(rs("file_1"),":")(1)
					  End if
					  state_yn = rs("state_yn")

					Else

					End If
					rs.close
					set rs = Nothing
					%>
					<div id="cate01" class="cate_contents">
						<table class="table_write">
							<colgroup>
								<col style="width: 280px;">
								<col style="width: *;">
								<col style="width: 510px;">
							</colgroup>
							<tbody>
								<tr>
									<th><em>*</em> 카테고리명</th>
									<td colspan="2"><input type="text" name="cate_name" id="cate_name" value="<%=cate_name%>"></td>
								</tr>
								<tr>
								  <th><em>*</em> 메인이미지 (996*300)</th>
								  <td>
									<div class="file_box">
										<input class="upload_name" disabled="disabled">
										<input type="file" id="cate_thumbs01" name="file_1" class="upload_hidden">
										<label for="cate_thumbs01">파일첨부</label>
									</div>
									<input type="hidden" name="ori_file" value="<%=file_1%>" />
								  </td>
								  <td><%=file_1%></td>
							    </tr>
								<tr>
									<th><em>*</em> 노출여부</th>
									<td class="chk_wrap" colspan="2">
										<div class="chk_box">
											<input id="exposure_y" type="radio" name="state_yn" value="Y" <%If state_yn = "Y" then%>checked<%End if%> />
											<label for="exposure_y"><span></span><p>노출</p></label>
										</div>
										<div class="chk_box">
											<input id="exposure_n" type="radio" name="state_yn" value="N" <%If state_yn = "N" then%>checked<%End if%> />
											<label for="exposure_n"><span></span><p>노출안함</p></label>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="btns">
				    <%if intSeq2 = "0" then%>
					<button type="button" class="btn_gray" onClick="this.form.reset();">취소</button>
					<%else%>
					<button type="button" class="btn_gray" onclick="javascript:category_delete('<%=intSeq2%>');">삭제</button>
					<%End if%>
					<button type="button" class="btn_blue" onClick="this.form.onsubmit();">저장</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<%
Call DbClose()
%>

<script language="javascript">
<!--
function frmRequestForm_Submit(frm){

  if (frm.cate_name.value == "" ) {
    alert("카테고리명을 입력하세요.");
	$("#cate_name").focus();
	return;
  }

  if (frm.file_1.value == "" && frm.ori_file.value == "") {
    alert("이미지를 입력하세요.");
	return;
  }

  if ( frm.state_yn[0].checked == false && frm.state_yn[1].checked == false ) {
    alert("노출여부를 체크하세요.");
	return;
  }

  <%if intSeq2 = "0" then%>
  frm.action = "category_add_ok.asp?<%=getstring("")%>";
  <%else%>
  frm.action = "category_edit_ok.asp?<%=getstring("")%>";
  <%end if%>

  frm.submit();
}
//-->
</script>