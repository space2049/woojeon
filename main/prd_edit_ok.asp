<% @CODEPAGE="65001" language="vbscript" %>
<% option Explicit %>
<!--#include virtual = "/Avanplus/_Function.asp"-->
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<%
Dim tablename
tablename = "goodslist"
Const UploadFolder = "/upload/goods"

'+++++++[0] 입력은 update 수정은 update 삭제는 delete 로 변경한다.
Dim update

set update =  new DextQueryClass
Call update.setUploadFolder(UploadFolder)


'+++++++[1] 넘어오는 값들을 확인해 본다.넘어오는 값확인후 반드시 주석처리로 바꾼다
'삭제시에는 아래 3값이 활성화 되어있으면 에러남
'=====================================================================
'Call update.ViewFormName()  	'[주석]폼네임명을 볼수있는 함수
'Call update.ViewTbaleGride()	'[주석테이블로 보기
'Call update.ViewCreateTable()	'[주석테이블쿼리
'=====================================================================
'+++++++[2] 넘어오는 값확인후 반드시 주석처리로 바꾼다



'+++++++[3] 수정과 삭제의 경우만 활설화 된다. 입력(update)때는 비활성화
Call update.setwhere(" intSeq = '"& request.QueryString("intSeq") &"' ")	'//수정과 삭제에서만 사용
'+++++++[3]_End


'// 기본으로 넘어오는 값 세팅
'Call update.setDefaultData("")

'+++++++[4] form에서 넘어오는 값 세팅 / 파일로 넘어오는 값 제외 / delete에서는 사용하지 않음[4번은 삭제]

Call update.setdataAdd("category_num", update.getFormValue("category_num"))
Call update.setdataAdd("goods_name", update.getFormValue("goods_name"))
Call update.setdataAdd("goods_code", update.getFormValue("goods_code"))

Call update.setdataAdd("state_yn", update.getFormValue("state_yn"))

Call update.setdataAdd("option_type1", update.getFormValue("option_type1"))
Call update.setdataAdd("option_name1", update.getFormValue("option_name1"))
Call update.setdataAdd("option_contents1", update.getFormValue("option_contents1"))

Call update.setdataAdd("option_type2", update.getFormValue("option_type2"))
Call update.setdataAdd("option_name2", update.getFormValue("option_name2"))
Call update.setdataAdd("option_contents2", update.getFormValue("option_contents2"))

Call update.setdataAdd("option_type3", update.getFormValue("option_type3"))
Call update.setdataAdd("option_name3", update.getFormValue("option_name3"))
Call update.setdataAdd("option_contents3", update.getFormValue("option_contents3"))

Call update.setdataAdd("option_type4", update.getFormValue("option_type4"))
Call update.setdataAdd("option_name4", update.getFormValue("option_name4"))
Call update.setdataAdd("option_contents4", update.getFormValue("option_contents4"))


'Call update.setdataAdd("dtmupdateDate", date())
'+++++++[4]_End

Call update.settablename(tablename)

Call DbOpen()


'+++++++[5] form에서 넘어오는 업로드될 파일 값 세팅 [함수명 바뀜update-update-delete]
	call update.setFileData("file_1","update")
	'Call update.setupdateNumFileDataAdd("sur_image")
'+++++++[5]_End


	dim strSQL
	strSQL = update.getqueryupdate	'[함수명 바뀜update-update-delete]			

	Call AdoConnExecute(strSQL)
	'response.Write("<br><br>"&strSQL)
Call DbClose()

set update = nothing


'+++++++[6] 메세지를 바꿔주고 처리후 이동할 페이지를 정해준다.
Call jsAlertMsgUrl("수정되었습니다", "./prd_list.asp?page="& request.QueryString("page"))
'+++++++[6]_End
%>