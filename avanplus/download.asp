<%@LANGUAGE="vbscript" CodePage="65001" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<!--#include virtual = "/Avanplus/_FormFunc.asp"-->
<!--#include virtual = "/Avanplus/_Function.asp"-->
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<%
Call dbopen()
sql4 = "SELECT img1 FROM notice WHERE intSeq = "&request.QueryString("intSeq")&""
set rs4 = dbconn.execute(sql4)

  img1 = rs4(0)

rs4.close
Set rs4=Nothing

call dbclose

l_file = Split(img1,":")(0)
u_file = Split(img1,":")(1)
l_folder = trim(Request.querystring("o_folder"))

target = "d:\origincell\www\upload\"&l_folder&"\"&l_file

Set Fso = Server.CreateObject("Scripting.FileSystemObject")

If Fso.FileExists(target) Then

    contentDisp = "attachment;filename="
    contentType = "application/unknown"
    l_file = Replace(l_file,",","")

    Response.Clear
    Response.AddHeader "Content-Transfer-Encoding", "binary"
    Response.AddHeader "Pragma", "no-cache"
    Response.AddHeader "Expires", "0" 

 '   If InStr(userAgent, "MSIE") = 0 Then
 '       Response.AddHeader "Content-Disposition", contentDisp & l_file
 '   Else
        Response.AddHeader "Content-Disposition", contentDisp & server.UrlPathEncode(u_file)
        Set objFile = Fso.GetFile(target)
'        Response.AddHeader "Content-Length", objFile.Size  '이 부분 주석처리해야 함
        Set objFile = Nothing  
 '   End If

    Set objStream = Server.CreateObject("ADODB.Stream")
    objStream.Open
    objStream.Type = 1
    objStream.LoadFromFile target
    Response.BinaryWrite objStream.Read
    Set objstream = Nothing

Else 

    showMsgBox "해당 파일이 존재하지 않습니다.","history.back();"

End If

Set Fso = Nothing

Sub ShowMsgBox(ByVal strMsg , ByVal strGoUrl)
 Response.Write "<script language='JavaScript'>" & vbCRLF

 If strMsg <> "" Then Response.Write " alert(""" & strMsg & """);" & vbCRLF
 If strGoUrl <> "" Then Response.Write " " & strGoUrl & vbCRLF

 Response.Write "</script>" & vbCRLF
 Response.End
End Sub

Function GetMIMEType(Extension)

  dim Ext
  Ext = UCase(Extension)
  select case Ext  'Common documents
   case "TXT", "TEXT", "JS", "VBS", "ASP", "CGI", "PL", "NFO", "ME", "DTD"
    sMIME = "text/plain"
   case "HTM", "HTML", "HTA", "HTX", "MHT"
    sMIME = "text/html"
   case "CSV"
    sMIME = "text/comma-separated-values"
   case "JS"
    sMIME = "text/javascript"
   case "CSS"
    sMIME = "text/css"
   case "PDF"
    sMIME = "application/pdf"
   case "RTF"
    sMIME = "application/rtf"
   case "XML", "XSL", "XSLT"
    sMIME = "text/xml"
   case "WPD"
    sMIME = "application/wordperfect"
   case "WRI"
    sMIME = "application/mswrite"
   case "XLS", "XLS3", "XLS4", "XLS5", "XLW", "XLSM"
    sMIME = "application/msexcel"
   case "DOC"
    sMIME = "application/msword"
   case "PPT","PPS"
    sMIME = "application/mspowerpoint"
   'WAP/WML files
   case "WML"
    sMIME = "text/vnd.wap.wml"
   case "WMLS"
    sMIME = "text/vnd.wap.wmlscript"
   case "WBMP"
    sMIME = "image/vnd.wap.wbmp"
   case "WMLC"
    sMIME = "application/vnd.wap.wmlc"
   case "WMLSC"
    sMIME = "application/vnd.wap.wmlscriptc"
   'Images
   case "GIF"
    sMIME = "image/gif"
   case "JPG", "JPE", "JPEG"
    sMIME = "image/jpeg"
   case "PNG"
    sMIME = "image/png"
   case "BMP"
    sMIME = "image/bmp"
   case "TIF","TIFF"
    sMIME = "image/tiff"
   case "AI","EPS","PS"
    sMIME = "application/postscript"
   'Sound files
   case "AU","SND"
    sMIME = "audio/basic"
   case "WAV"
    sMIME = "audio/wav"
   case "RA","RM","RAM"
    sMIME = "audio/x-pn-realaudio"
   case "MID","MIDI"
    sMIME = "audio/x-midi"
   case "MP3"
    sMIME = "audio/mp3"
   case "M3U"
    sMIME = "audio/m3u"
   'Video/Multimedia files
   case "ASF"
    sMIME = "video/x-ms-asf"
   case "AVI"
    sMIME = "video/avi"
   case "MPG","MPEG"
    sMIME = "video/mpeg"
   case "QT","MOV","QTVR"
    sMIME = "video/quicktime"
   case "SWA"
    sMIME = "application/x-director"
   case "SWF"
    sMIME = "application/x-shockwave-flash"
   'Compressed/archives
   case "ZIP"
    sMIME = "application/x-zip-compressed"
   case "GZ"
    sMIME = "application/x-gzip"
   case "RAR"
    sMIME = "application/x-rar-compressed"
   'Miscellaneous
   case "COM","EXE","DLL","OCX"
    sMIME = "application/octet-stream"
   'Unknown (send as binary stream)
   case else
    sMIME = "application/octet-stream"
  end select
  GetMimeType = sMIME

End Function
%>